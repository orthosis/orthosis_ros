# HOPE ROS Packages  
This repository contains the files for all ROS based simulation packages for the
 HOPE Hand exoskeleton developed at the WPI AIM Lab.

## Installation  
The following dependencies should be installed. The packages have only been 
tested on **ROS Melodic Morenia and Ubuntu 18.04 Bionic Beaver**. Please follow
 the required steps to install 
 [ROS Melodic Morenia](http://wiki.ros.org/melodic/Installation/Ubuntu). 
 Installing **ROS-DESKTOP-FULL** will be the most suited.  

Other dependencies:
```bash
sudo apt install ros-melodic-joint-state-*
sudo apt-get install ros-melodic-joint-state-controller \
ros-melodic-effort-controllers ros-melodic-position-controllers
sudo apt install ros-melodic-ros-controllers ros-melodic-ros-control
sudo apt install ros-melodic-controller-*
sudo apt install ros-melodic-gazebo-*
sudo apt install ros-melodic-control*
sudo apt install clang-format
sudo apt install ros-melodic-urdf*
```

Now clone this repository in your catkin_ws and build and source the files.

```bash
cd ~/catkin_ws/src
git clone https://gitlab.com/orthosis/orthosis_ros.git
cd ~/catkin_ws
catkin_make
source devel/setup.bash
```

## Running the simulation using Gazebo

Start a `rosmaster`  

```bash
roscore
```
## Update (03/10): Using RQT plugin instead of spawning multiple terminals

```bash
rosrun rqt_gui rqt_gui
```

Click on plugins -> orthosis

## Old Technique

In a new terminal, launch the simulation in gazebo  

```bash
source ~/catkin_ws/devel/setup.bash
cd ~/catkin_ws/src/orthosis_ros/orthosis_description/scripts/
sudo chmod u+x ./pysim.py
rosrun orthosis_description pysim.py
```

In a new terminal, launch the gazebo ros controllers  

```bash
source ~/catkin_ws/devel/setup.bash
cd ~/catkin_ws/src/orthosis_ros/orthosis_control/scripts/
sudo chmod u+x ./controllers.py
rosrun orthosis_control controllers.py
```

In a new terminal, launch the control algorithm

```bash
source ~/catkin_ws/devel/setup.bash
cd ~/catkin_ws/src/orthosis_ros/orthosis_control/scripts/
sudo chmod u+x ./control.py
rosrun orthosis_control control.py 0
```

(The argument 0 implies simulation mode)

## Contributing to the controller
**Update: Pushed RBDL fork to rbdl-orb master (commit: b54d70a5a1f56f8530f3389d65f1d00b2d970e44)**: 
Please use the official rbdl installation now.  
[rbdl](https://github.com/ORB-HD/rbdl-orb)  


~~It is important to install [rbdl](https://gitlab.com/orthosis/rbdl) to write 
controllers for this robot.~~

## Intent Detection  

To launch realsense camera:
```bash
git clone https://github.com/IntelRealSense/realsense-ros.git
cd realsense-ros/
git checkout `git tag | sort -V | grep -P "^\d+\.\d+\.\d+" | tail -1
cd ..
catkin_make

roslaunch realsense2_camera rs_camera.launch

# align depth stream with rgb
roslaunch realsense2_camera rs_camera.launch align_depth:=true
```

(ref: https://github.com/IntelRealSense/realsense-ros)

To run object detection:
```bash
cd orthosis_intent/src
python3 examples/detector_vid.py
```
