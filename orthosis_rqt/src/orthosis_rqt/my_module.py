# import os
import rospy
import rostopic
# import rospkg
import roslaunch
import rosnode

from qt_gui.plugin import Plugin
# from python_qt_binding import loadUi
from python_qt_binding import QtCore, QtGui

import dynamic_reconfigure.client

from nav_msgs.msg import Odometry

import numpy as np
import pyqtgraph as pg
import time

from std_msgs.msg import Int64, Float32MultiArray, Float32, Float64, Bool


class MyPlugin(Plugin):

  def __init__(self, context):
    super(MyPlugin, self).__init__(context)
    # Give QObjects reasonable names
    # self.setObjectName('MyPlugin')
    self.setObjectName('OrthosisPlugin')

    # Process standalone plugin command-line arguments
    from argparse import ArgumentParser
    parser = ArgumentParser()
    # Add argument(s) to the parser.
    parser.add_argument(
      "-q",
      "--quiet",
      action="store_true",
      dest="quiet",
      help="Put plugin in silent mode"
    )
    args, unknowns = parser.parse_known_args(context.argv())
    if not args.quiet:
      print('arguments: ', args)
      print('unknowns: ', unknowns)

    # Create QWidget
    self._widget = MainGui()

    # Add widget to the user interface
    context.add_widget(self._widget)

    return

  def shutdown_plugin(self):
    # TODO unregister all publishers here
    pass

  def save_settings(self, plugin_settings, instance_settings):
    # TODO save intrinsic configuration, usually using:
    # instance_settings.set_value(k, v)
    pass

  def restore_settings(self, plugin_settings, instance_settings):
    # TODO restore intrinsic configuration, usually using:
    # v = instance_settings.value(k)
    pass

  # def trigger_configuration(self):
  # Comment in to signal that the plugin has a way to configure. This will
  # enable a setting button (gear icon) in each dock widget title
  # bar. Usually used to open a modal configuration dialog


run_sim_flag = False


class MainGui(QtGui.QWidget):
  """ Main widget for Orthosis GUI """

  def __init__(self):
    super(MainGui, self).__init__()
    self.setObjectName("OrthosisGUI")
    self.setWindowTitle("Orthosis GUI")

    # Elements
    self.tabs = QtGui.QTabWidget()
    self.tabs.insertTab(0, GUI(), "GUI")

    # Layout
    self.layout = QtGui.QVBoxLayout()
    self.layout.addWidget(self.tabs)
    self.setLayout(self.layout)
    self.tabs_count = 0


class GUI(QtGui.QWidget):
  """ Tuning interface """

  def __init__(self):
    super(GUI, self).__init__()

    self.launch = roslaunch.scriptapi.ROSLaunch()
    self.launch.start()
    self.layout = QtGui.QVBoxLayout()

    self.layout_sim = QtGui.QHBoxLayout()
    self.cmd_run_sim = "Run Simulation"
    self.btn_run_sim = QtGui.QPushButton(self.cmd_run_sim)
    self.btn_run_sim.clicked.connect(self.run_sim)
    self.layout_sim.addWidget(self.btn_run_sim)

    self.cmd_stop_sim = "Stop Simulation"
    self.btn_stop_sim = QtGui.QPushButton(self.cmd_stop_sim)
    self.btn_stop_sim.clicked.connect(self.stop_sim)
    self.layout_sim.addWidget(self.btn_stop_sim)
    self.layout.addLayout(self.layout_sim)

    self.layout_hw = QtGui.QHBoxLayout()
    self.cmd_run_hw = "Start Teleop"
    self.btn_run_hw = QtGui.QPushButton(self.cmd_run_hw)
    self.btn_run_hw.clicked.connect(self.run_hw)
    self.layout_hw.addWidget(self.btn_run_hw)

    self.cmd_stop_hw = "Stop Teleop"
    self.btn_stop_hw = QtGui.QPushButton(self.cmd_stop_hw)
    self.btn_stop_hw.clicked.connect(self.stop_hw)
    self.layout_hw.addWidget(self.btn_stop_hw)
    self.layout.addLayout(self.layout_hw)

    self.tabs = QtGui.QTabWidget()
    self.layout.addWidget(self.tabs)
    self.tabs.insertTab(0, TuningMotor(), "Motor")
    # self.tabs.insertTab(1, Visualisation(), "Visualisation")
    self.tabs.setTabEnabled(0, True)
    # self.tabs.setTabEnabled(1, False)
    self.setLayout(self.layout)

    self.keep_sim_active = False
    self.keep_hw_active = False

    self.teleop_pub = rospy.Publisher("/teleop_state", Bool, queue_size=10)
    self.sim_pub = rospy.Publisher("/sim_state", Bool, queue_size=10)
    self.teleop = False

    return

  def run_sim(self, namespace="/"):
    """ Run sim """
    if self.keep_sim_active:
      print "SIM ALREADY RUNNING, TO RESTART STOP AND THEN START"
      return

    if self.keep_hw_active:
      print "STOPPING TELEOP, PLEASE RESTART AGAIN"
      self.stop_hw(namespace)
      time.sleep(2)

    self.keep_sim_active = True
    self.sim_pub.publish(self.keep_sim_active)
    rospy.set_param("/orthosis/is_teleop", self.teleop)
    rospy.set_param("/orthosis/is_sim_active", self.keep_sim_active)
    print(self.cmd_run_sim)

    node = roslaunch.core.Node(
      package="orthosis_description",
      node_type="pysim.py",
      name="pysim",
      namespace=namespace,
      respawn=False
    )
    self.process_sim = self.launch.launch(node)
    time.sleep(2)
    node = roslaunch.core.Node(
      package="orthosis_control",
      node_type="controllers.py",
      name="controllers",
      namespace=namespace,
      respawn=False
    )
    self.process_controllers = self.launch.launch(node)
    time.sleep(2)

    if not self.keep_hw_active:
      print "LAUNCHING SIM MODE"
      node = roslaunch.core.Node(
        package="orthosis_control",
        node_type="control.py",
        name="control",
        namespace=namespace,
        respawn=False,
        args="0"
      )
      self.process_control = self.launch.launch(node)

      self.tabs.setTabEnabled(0, True)

    return

  def stop_sim(self, namespace="/"):
    """ Stop sim """
    print("Stopping Simulation")
    self.keep_sim_active = False
    self.sim_pub.publish(self.keep_sim_active)
    rospy.set_param("/orthosis/is_sim_active", self.keep_sim_active)
    self.process_control.stop()
    time.sleep(2)
    self.tabs.setTabEnabled(0, False)
    # Test if simulation and controllers shut down without having to dump topics
    self.process_controllers.stop()
    time.sleep(4)
    self.process_sim.stop()
    time.sleep(2)
    rospy.delete_param("/orthosis")
    nodes = rosnode.get_node_names()
    for node in nodes:
      if "rqt" in node:
        nodes.remove(node)

    rosnode.kill_nodes(nodes)
    return

  def run_hw(self, namespace="/"):
    """ Run sim """
    if self.keep_sim_active:
      self.stop_sim()

    if self.keep_hw_active:
      print "HW ALREADY RUNNING, TO RESTART STOP AND THEN START"
      return
    print(self.cmd_run_hw)
    self.keep_hw_active = True
    self.teleop = True
    rospy.set_param("/orthosis/is_teleop", self.teleop)
    self.teleop_pub.publish(self.teleop)

    node = roslaunch.core.Node(
      package="orthosis_description",
      node_type="core_bringup.py",
      name="core",
      namespace=namespace,
      respawn=False
    )
    self.process_core_bringup = self.launch.launch(node)
    time.sleep(2)
    node = roslaunch.core.Node(
      package="orthosis_control",
      node_type="flex_to_joint_adapter.py",
      name="flex_to_joint_adapter",
      namespace=namespace,
      respawn=False
    )
    self.process_flex_to_joint_adapter = self.launch.launch(node)
    time.sleep(2)
    node = roslaunch.core.Node(
      package="orthosis_control",
      node_type="control.py",
      name="control",
      namespace=namespace,
      respawn=False,
      args="1"
    )
    self.process_control = self.launch.launch(node)
    time.sleep(2)
    node = roslaunch.core.Node(
      package="rviz",
      node_type="rviz",
      name="rviz",
      namespace=namespace,
      respawn=False,
      args="-d $(find orthosis_description)/rviz/default.rviz"
    )
    self.rviz = self.launch.launch(node)

    self.tabs.setTabEnabled(0, True)

    return

  def stop_hw(self, namespace="/"):
    """ Stop sim """
    print("Stopping Teleoperation")
    self.keep_hw_active = False
    self.teleop = False
    rospy.set_param("/orthosis/is_teleop", self.teleop)
    self.teleop_pub.publish(self.teleop)
    # if not self.keep_sim_active:
    self.tabs.setTabEnabled(0, False)
    self.rviz.stop()
    time.sleep(2)
    self.process_control.stop()
    time.sleep(2)
    self.process_flex_to_joint_adapter.stop()
    time.sleep(2)
    self.process_core_bringup.stop()
    time.sleep(2)
    rospy.delete_param("/orthosis")
    nodes = rosnode.get_node_names()
    for node in nodes:
      if "rqt" in node:
        nodes.remove(node)

    rosnode.kill_nodes(nodes)
    return


class TuningMotor(QtGui.QWidget):
  """ Widget for tuning Motors """

  def __init__(self):
    super(TuningMotor, self).__init__()

    # Elements
    self.scrollarea = QtGui.QScrollArea()
    self.motor_pinky_finger = TuningMotorV("pinky")
    self.motor_ring_finger = TuningMotorV("ring")
    self.motor_middle_finger = TuningMotorV("middle")
    self.motor_index_finger = TuningMotorV("index")
    # self.motor_thumb_finger = TuningMotorV("Thumb_Finger")
    self.btn_publish = QtGui.QCheckBox("Publish Positions")
    self.btn_publish.toggled.connect(self.publish)

    # Layout
    layout = QtGui.QVBoxLayout()

    layout_motors = QtGui.QVBoxLayout()
    motors = QtGui.QWidget()
    layout_motors.addWidget(self.motor_index_finger)
    layout_motors.addWidget(self.motor_middle_finger)
    layout_motors.addWidget(self.motor_ring_finger)
    layout_motors.addWidget(self.motor_pinky_finger)

    # layout_motors.addWidget(self.motor_thumb_finger)
    motors.setLayout(layout_motors)
    self.scrollarea.setWidget(motors)
    layout.addWidget(self.scrollarea)
    layout.addWidget(self.btn_publish)
    self.setLayout(layout)
    return

  def publish(self, a):
    if (self.btn_publish.isChecked()):
      for finger in [
        self.motor_pinky_finger,
        self.motor_ring_finger,
        self.motor_middle_finger,
        self.motor_index_finger,
        # self.motor_thumb_finger
      ]:
        finger.e.ispub = 1
    else:
      for finger in [
        self.motor_pinky_finger,
        self.motor_ring_finger,
        self.motor_middle_finger,
        self.motor_index_finger,
        # self.motor_thumb_finger
      ]:
        finger.e.ispub = 0
    return


class TuningMotorV(QtGui.QWidget):
  """ Motor Values tuning widget """

  def __init__(self, name):
    super(TuningMotorV, self).__init__()

    # Elements
    self.e = SliderM("Extension", 999, 1, name)

    # Layout
    layout = QtGui.QVBoxLayout()
    layout.addWidget(QtGui.QLabel("{}:".format(name)))
    layout.addWidget(self.e)
    self.setLayout(layout)
    return


class SliderM(QtGui.QWidget):
  """ Slider for tuning Motor position """

  def __init__(self, element, valuemax, valuemin, name):
    super(SliderM, self).__init__()

    # Variables
    self.topic = "/orthosis/{}".format(name)
    print self.topic
    self.pub = rospy.Publisher(self.topic, Float32, queue_size=10)

    # Elements
    self.slider = QtGui.QSlider(QtCore.Qt.Horizontal)
    self.slider.setMaximum(valuemax)
    self.slider.setMinimum(valuemin)
    self.slider.setValue(valuemax)
    self.spinmax = QtGui.QDoubleSpinBox()
    self.spinmax.setMaximum(1e6)
    self.spinmax.setMinimum(-1e6)
    self.spinmax.setDecimals(4)
    self.spinmax.setValue(valuemax)
    self.spinmin = QtGui.QDoubleSpinBox()
    self.spinmin.setMaximum(valuemax)
    self.spinmin.setMinimum(valuemin)
    self.spinmin.setDecimals(4)
    self.spinmin.setValue(valuemax)
    self.text = QtGui.QLabel()
    self.text.setText("999")
    self.slider.valueChanged.connect(self.sliderchangeval)
    self.spinmax.valueChanged.connect(self.setmax)
    self.spinmin.valueChanged.connect(self.setmin)

    # Layout
    layout = QtGui.QHBoxLayout()
    layout.addWidget(QtGui.QLabel("Finger Position"))
    layout.addWidget(self.spinmin)
    layout.addWidget(QtGui.QLabel("{}:".format(element)))
    layout.addWidget(self.slider)
    layout.addWidget(QtGui.QLabel("Flexion"))
    # layout.addWidget(self.spinmax)
    layout.addWidget(QtGui.QLabel("Value"))
    layout.addWidget(self.text)
    self.setLayout(layout)
    return

  # Have this change on slider change
  def sliderchangeval(self, a):
    self.text.setText(str(self.slider.value()))
    self.spinmin.setValue(self.slider.value())
    # if (self.ispub):
    force = self.slider.value()
    self.pub.publish(force)
    return

  def setmax(self, a):
    self.slider.setMaximum(self.spinmax.value())
    return

  def setmin(self, a):
    self.slider.setValue(self.spinmin.value())
    return


class Visualisation(QtGui.QWidget):
  """ Visualisation GUI """

  def __init__(self):
    super(Visualisation, self).__init__()

    # Elements
    tabs = QtGui.QTabWidget()
    tabs.addTab(VisualisationGraphs(), "Graphs")

    # Layout
    layout = QtGui.QVBoxLayout()
    layout.addWidget(tabs)
    self.setLayout(layout)
    return


class VisualisationGraphs(QtGui.QWidget):
  """ Visualisation GUI """

  def __init__(self):
    super(VisualisationGraphs, self).__init__()

    # Elements
    self.tree_topics = TopicsTree()
    self.tree_topics.model.itemChanged.connect(self.item_changed)
    self.graphs = []

    # Layout
    self.layout = QtGui.QVBoxLayout()
    self.splitter = QtGui.QSplitter()
    self.splitter.addWidget(self.tree_topics)
    self.splitter_graphs = QtGui.QSplitter(QtCore.Qt.Vertical)
    self.splitter.addWidget(self.splitter_graphs)
    self.layout.addWidget(self.splitter)
    self.setLayout(self.layout)
    return

  def item_changed(self, item):
    """ Item changed in topic tree """
    if item.checkState():
      self.graphs.append(
        [VisualisationGraph(item.text(),
                            item.text()),
         item.text()]
      )
      self.splitter_graphs.addWidget(self.graphs[-1][0])
    else:
      for i, graph in enumerate(self.graphs):
        if graph[1] == item.text():
          graph[0].close()
          self.layout.removeWidget(graph[0])
          graph[0].deleteLater()
          graph[0] = None
          self.graphs.pop(i)
      # if layout is not None:
      #     while layout.count():
      #         item = layout.takeAt(0)
      #         widget = item.widget()
      #         if widget is not None:
      #             widget.deleteLater()
      #         else:
      #             self.clearLayout(item.layout())
    return


class TopicsTree(QtGui.QWidget):
  """ Topics tree for selecting topics """

  def __init__(self):
    super(TopicsTree, self).__init__()

    # Elements
    self.tree = QtGui.QTreeView()
    self.model = QtGui.QStandardItemModel(self.tree)
    topics = rospy.get_published_topics()
    # Get all topics types
    topic_types = [
      topic_type for topic_type in list(set([topic for _,
                                             topic in topics]))
    ]
    for topic_type in topic_types:
      item_topic_type = QtGui.QStandardItem(topic_type)
      item_topic_type.setCheckable(True)
      for food, topic_type2 in topics:
        if topic_type2 == topic_type:
          item_topic = QtGui.QStandardItem(food)
          item_topic.setCheckable(True)
          item_topic_type.appendRow(item_topic)
      self.model.appendRow(item_topic_type)
    self.tree.setModel(self.model)

    # Layout
    layout = QtGui.QVBoxLayout()
    layout.addWidget(self.tree)
    self.setLayout(layout)
    return


class VisualisationGraph(QtGui.QWidget):
  """ Graph visualisation """

  def __init__(self, name, topic):
    super(VisualisationGraph, self).__init__()

    # Elements
    self.graph = pg.PlotWidget()
    self.graph.showGrid(x=True, y=True, alpha=1)
    self.buffer_size_default = 1000
    self.buffer_size = self.buffer_size_default
    self.data = np.zeros([3, self.buffer_size, 2])
    self.iteration = 0

    # ROS
    self.msg_class, _, _ = rostopic.get_topic_class(topic)
    print(topic, self.msg_class)
    self.sub = rospy.Subscriber(topic, self.msg_class, self.callback)

    # Layout
    layout = QtGui.QVBoxLayout()
    layout.addWidget(QtGui.QLabel(name))
    layout.addWidget(self.graph)
    self.setLayout(layout)

    return

  def callback(self, data):
    """ ROS callback """
    if self.iteration + 1 > self.buffer_size:
      self.data = np.concatenate(
        [self.data,
         np.zeros([3,
                   self.buffer_size_default,
                   2])],
        axis=1
      )
      self.buffer_size += self.buffer_size_default
    self.data[0,
              self.iteration,
              0] = (
                data.header.stamp.secs + 1e-9 * data.header.stamp.nsecs
              )
    self.data[0, self.iteration, 1] = data.pose.pose.position.x
    if self.iteration > 0:
      self.graph.plot(x=[1, 5, 10], y=[30, 10, 80])

    self.iteration += 1
    return

  def close(self):
    """ Close """
    self.sub.unregister()
    return
