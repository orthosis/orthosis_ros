#!/usr/bin/env python

import sys

from orthosis_rqt.my_module import MyPlugin
from rqt_gui.main import Main

plugin = 'orthosis_rqt'
main = Main(filename=plugin)
sys.exit(main.main(standalone=plugin))
