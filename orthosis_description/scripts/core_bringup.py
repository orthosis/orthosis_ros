#!/usr/bin/python
""" This script spawns multiple models based on the number of islands and robots
 specified.

"""

__author__ = "Dhruv Kool Rajamani"
__email__ = "dkoolrajamani@wpi.edu"
__copyright__ = "Copyright 2019"
__date__ = "8 October 2019"

from urdf_parser import parse_model
import time
import rospy
import rospkg
import roslaunch
import subprocess
import os
import sys

from generate_urdf import get_continous_joints, generate_robot, generate_urdf
from generate_urdf import get_all_continuous_joints_names, add_gazebo_plugins
from generate_urdf import add_transmission_plugins, add_rgbd_camera
from urdf_parser import get_all_joints, get_p_and_c_links, scale_robot_inertia


def setupRobots(namespace='/', model_name='orthosis'):
  """ Setup nodes and params """

  print namespace

  # rospy.set_param("namespace_{}_{}".format(island_id, robot_id), namespace)
  rospy.set_param("namespace", namespace)

  rospack = rospkg.RosPack()
  rospack.list()

  root_path = rospack.get_path('orthosis_description')
  model_path = root_path + "/urdf/orthosis_description_barebones.urdf"
  # unscaled_arm_mount_model_path = root_path + "/urdf/arm_mount_description_unscaled.urdf"
  # model_path = root_path + "/urdf/orthosis_description.urdf"
  # arm_mount_model_path = root_path + "/urdf/arm_mount_description.urdf"

  robot = parse_model(model_path)

  # scale = 100
  # tof_cam_link = "base_plate"
  # (robot, camera_link_name) = add_rgbd_camera(robot=robot,
  #                                             scale=scale,
  #                                             parent_link_name=tof_cam_link)
  # camera_link_name = tof_cam_link + "_camera"
  # robot = scale_robot_inertia(robot, scale)

  # rospy.set_param(namespace + "scale", scale)

  # print robot.to_xml_string()

  # if os.path.isfile(model_path):
  #   os.remove(model_path)

  # with open(model_path, "w+") as f:
  #   f.write(robot.to_xml_string())
  #   f.close()

  # print(robot.to_xml_string())

  # robot = add_transmission_plugins(robot)

  # robot = add_gazebo_plugins(
  #   robot=robot,
  #   namespace=namespace,
  #   use_ft_sensors=True,
  #   use_p3d_sensors=True,
  #   fix_base_joint={
  #     "child": "arm_base_link",
  #     "origin": {
  #       "xyz": [0,
  #               0,
  #               0.8],
  #       "rpy": [0,
  #               0,
  #               3.1419]
  #     }
  #   },
  #   use_rgbd_camera={"link": camera_link_name}
  # )

  robot.name = model_name  # robotNamespace

  # print(robot.to_xml_string())

  rospy.set_param(namespace + "robot_description", robot.to_xml_string())

  # log_path = rospack.get_path('inverted-pendulum_analysis')

  # rospy.set_param(
  #     "log_path",
  #     log_path
  # )

  joints = get_all_joints(robot)

  for joint in robot.joints:
    if joint.limit is not None:
      rospy.set_param(
        namespace + "joint/limits/{}".format(joint.name),
        [
          joint.limit.lower,
          joint.limit.upper,
          joint.limit.velocity,
          joint.limit.effort
        ]
      )

  config_joints = ["index_joint1", "index_joint2"]
  rospy.set_param(namespace + "hw_config_joints", config_joints)

  rospy.set_param(namespace + "all_joints", joints)

  mov_joints = [
    joint for typ,
    joint in joints.items() if typ in ['revolute',
                                       'continuous',
                                       'prismatic']
  ]

  m_joints = []
  for ls in mov_joints:
    if ls is not None:
      for item in ls:
        m_joints.append(item)
    else:
      continue

  for typ, joints in joints.items():
    if joints is not None:
      for joint_i, joint in enumerate(joints):
        rospy.set_param(
          namespace + "joints/{}/{}".format(typ,
                                            joint_i),
          get_p_and_c_links(robot,
                            joint)
        )

  arg = namespace + "n_joints"
  n_joints = len(m_joints)
  rospy.set_param(arg, n_joints)

  # cam_robot = cam_generate_robot(namespace)
  # rospy.set_param(
  #         namespace + "camera_description",
  #         cam_robot.to_xml_string()
  # )

  # for joint_i, joint in enumerate(m_joints):
  #     rospy.set_param(
  #         )
  rospy.set_param(namespace + "parameters/control/joints/", m_joints)

  node_state = roslaunch.core.Node(
    package="robot_state_publisher",
    node_type="robot_state_publisher",
    name="robot_state_publisher",
    namespace=namespace
  )

  d = {
    "joint_state_controller":
      {
        "type": "joint_state_controller/JointStateController",
        "publish_rate": 100
      }
  }

  add = namespace + "control/config/"
  rospy.set_param(add, d)

  node_joint_state = roslaunch.core.Node(
    package="controller_manager",
    node_type="spawner",
    name="controller_spawner",
    namespace=namespace,
    respawn=False,
    output="screen",
    args=(add + "joint_state_controller")
  )

  nodes = [node_state]
  return nodes


def launch_sim():
  """ Launch simulation with Gazebo """
  nodes = []
  processes = []
  launch = roslaunch.scriptapi.ROSLaunch()
  launch.start()

  nodes.extend(setupRobots("/orthosis/", "orthosis"))

  rospack = rospkg.RosPack()
  rospack.list()
  root_path = rospack.get_path('orthosis_description')
  with open(root_path + "/launch/core_bringup.launch", "w+") as f:
    for node in nodes:
      f.write(node.to_xml())

  f.close()

  processes = [launch.launch(node) for node in nodes]

  # Loop
  while any([process.is_alive() for process in processes]):
    time.sleep(2)
  # Close
  for process in processes:
    process.stop()
  return


def main():
  """ Main """
  launch_sim()
  return


if __name__ == '__main__':
  main()
