#!/usr/bin/python

import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import time
import csv
import numpy as np
import urllib
import math
import scipy
import os
import sys
from pprint import pprint
import rospkg
from scipy import signal
#from pydtw import dtw2d, dtw1d

# HERE'S HOW IT WORKS:
# TO GENERATE SYNCED DATASETS AS CSV FILES, RUN csv_parser.main() WITH NO
# ARGUMENTS. THIS WILL SAVE A SYNCED VERSION OF EACH TAKE IN THE RESPECTIVE
# FOLDERS IN ./LOGS AS Sync_Take_ FOR THE THETA1 AND THETA2 VALUES AND
# Sync_flex_ FOR THE BEND AND STRETCH SENSOR VALUES.
# THIS DOES NOT RETURN ANY VALUES

# TO GET SPECIFIC DATASETS IN ARRAY FORM, RUN csv_parser.main([4,5,..])
# WITH THE ARGUMENTS AS A LIST OF THE EXPERIMENTS YOU NEED. THE FUNCTION RETURNS
# TWO VALUES AS X WHICH CONTAINS A CONDENSED DATASET OF BEND AND STRETCH FOR
# ALL REQUIRED EXPERIMENTS AND Y WHICH CONTAINS A CONDENSED DATASET OF THETA1
# AND THETA2 FOR ALL REQUIRED EXPERIMENTS.

# rospack = rospkg.RosPack()
# rospack.list()

# root_path = rospack.get_path('orthosis_analysis')
root_path = '/home/dhruv/catkin_ws/src/orthosis_ros/orthosis_analysis'
np.set_printoptions(suppress=True)
base_path = root_path + '/logs'


def get_root_path():

  return root_path


def lag_finder(y1, y2, sr):
  n = len(y1)

  corr = signal.correlate(
    y2,
    y1,
    mode='same'
  ) / np.sqrt(
    signal.correlate(y1,
                     y1,
                     mode='same')[int(n / 2)] *
    signal.correlate(y2,
                     y2,
                     mode='same')[int(n / 2)]
  )

  delay_arr = np.linspace(-0.5 * n / sr, 0.5 * n / sr, n)
  delay = delay_arr[np.argmax(corr)]

  return int(delay)


class Experiment(object):

  def __init__(self, exp_data, exp_id=0):
    super(Experiment, self).__init__()
    self._len = len(exp_data)
    self._indexes = exp_data[0:self._len][0]
    self._stamps = exp_data[0:self._len][1]
    self._truths = np.ndarray((self._len, 2))
    self._inputs = np.ndarray((self._len, 2))
    self._exp_id = exp_id
    self._exp_data = exp_data

    for i in range(self._len):
      self._truths[i][0] = exp_data[i][2]
      self._truths[i][1] = exp_data[i][3]
      self._inputs[i][0] = exp_data[i][4]
      self._inputs[i][1] = exp_data[i][5]

  def __repr__(self):
    super(Experiment, self).__repr__()
    return {
      'exp_id': self._exp_id,
      'len': self._len,
      'indexes': self._indexes,
      'stamps': self._stamps,
      'truths': [_truths for _truths in self._truths],
      'inputs': [_inputs for _inputs in self._inputs]
    }

  def __str__(self):
    super(Experiment, self).__str__()
    return 'Experiment(exp_id=' + str(self._exp_id) + ')'

  def to_string(self):
    return pprint(self._exp_data)


def get_sub_directories(path=None):
  if path is not None:
    dirs = [x[0] for x in os.walk(path)]
  else:
    dirs = [x[0] for x in os.walk(base_path)]
  dirs = sorted(dirs[1:])
  directories = {i + 1: d for i, d in enumerate(dirs)}

  return directories


def get_files(path=None):
  if path is not None:
    dirs = [x[2] for x in os.walk(path)]
  else:
    dirs = [x[2] for x in os.walk(base_path)]
  dirs = sorted(dirs[1:])
  files = {i + 1: d for i, d in enumerate(dirs)}

  return files


def sync_time(take_vals, flex_vals, tol=12):

  flex_time = []
  take_time = []

  for i, val in enumerate(flex_vals):
    flex_time.append(datetime.fromtimestamp(val[0] / 1000))

  for i, val in enumerate(take_vals):
    take_time.append(datetime.utcfromtimestamp(val[0] / 1000))

  synced_time = []
  for fi, fval in enumerate(flex_time):
    if fi is not 0:
      times = [
        i for i,
        t in enumerate(take_time)
        if t.minute == fval.minute and t.second == fval.second and
        (t.microsecond - fval.microsecond) in range(0,
                                                    12 * 1000)
      ]
      #print times
      if len(times) > 0:
        synced_time.append(times[0])

  #print synced_time

  return synced_time


def manipcsv(test_number=1):
  directories = get_sub_directories()
  #print 'Experiments to select from: '
  #for i in directories.keys():
  #  print i
  flex_csv_path = str(directories[test_number]) + '/flex.csv'
  take_csv_path = str(directories[test_number]
                      ) + '/Take_{}_thetas.csv'.format(test_number)

  f_vals = importcsv(flex_csv_path)
  t_vals = importcsv(take_csv_path)
  flex_vals = np.ndarray((len(f_vals), 3))
  take_vals = np.ndarray((len(t_vals) - 2, 3))

  for i, row in enumerate(f_vals):
    for j, ele in enumerate(row):
      if i == 0 and ele == 0:
        continue
      flex_vals[i][j] = ele

  take_start_time = 0
  for i, row in enumerate(t_vals):
    for j, ele in enumerate(row):
      if i >= 2:
        if j == 0:
          tm = take_start_time + (float(ele) * 1000)
          take_vals[i - 2][j] = int(tm)
        else:
          take_vals[i - 2][j] = ele
      else:
        if i == 1:
          take_start_time = unix_time_millis(
            datetime.strptime(row[0],
                              '%Y-%M-%d %H:%m:%S.%f')
          )
        continue

  synced_time = sync_time(take_vals, flex_vals)
  # take_time = []
  # flex_time = []
  # for i, val in enumerate(take_vals):
  #   # take_vals[i][0] = unix_time_millis(
  #   #   datetime.utcfromtimestamp(val[0] / 1000)
  #   # )
  #   take_time.append(datetime.utcfromtimestamp(val[0] / 1000))

  # for i, val in enumerate(flex_vals):
  #   # flex_vals[i][0] = unix_time_millis(datetime.fromtimestamp(val[0] / 1000))
  #   flex_time.append(datetime.fromtimestamp(val[0] / 1000))

  # # synced_time = sync_time(take_vals, flex_vals)
  # # for fi, fval in enumerate(flex_time):
  # take_time_delta = 0
  # for i, t in enumerate(take_time):
  #   if t.minute == flex_time[1].minute and t.second == flex_time[1].second:
  #     if np.abs(t.microsecond -
  #               flex_time[1].microsecond) in range(0,
  #                                                  12 * 1000):
  #       take_time_delta = i

  # flex_vals = np.delete(flex_vals, obj=0, axis=0)
  # take_vals = np.delete(
  #   take_vals,
  #   obj=[i for i in range(take_time_delta)],
  #   axis=0
  # )
  # print "delta " + str(take_vals[0][0] - flex_vals[0][0])
  # print "size\n\t" + str(np.shape(take_vals)
  #                        ) + "\n\t" + str(np.shape(flex_vals))

  # cost_matrix, cost, alignment_flex, alignment_take = dtw2d(flex_vals, take_vals)

  # tm = 0
  # final_mat = [[0, 0., 0., 0., 0., 0.] for i in alignment_flex]
  # for i, (id_flex, id_take) in enumerate(zip(alignment_flex, alignment_take)):
  #   final_mat[i][0] = tm
  #   final_mat[i][1] = take_vals[id_take][0]
  #   final_mat[i][2] = take_vals[id_take][1]
  #   final_mat[i][3] = take_vals[id_take][2]
  #   final_mat[i][4] = flex_vals[id_flex][1]
  #   final_mat[i][5] = flex_vals[id_flex][2]
  #   tm += 1

  final_mat = [[0, 0., 0., 0., 0., 0.] for i in synced_time]
  for i, index in enumerate(synced_time):
    final_mat[i][0] = i
    final_mat[i][1] = take_vals[index][0]
    final_mat[i][2] = take_vals[index][1]
    final_mat[i][3] = take_vals[index][2]
    final_mat[i][4] = flex_vals[i + 1][1]
    final_mat[i][5] = flex_vals[i + 1][2]

  return final_mat


# test indexing starts from 1
def importcsv(path):
  rows = []
  with open(path) as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
      rows.append(row)

  return rows


epoch = datetime.utcfromtimestamp(0)


def exportcsv(data, name, path):
  # field names
  if str(name) == 'x':
    fields = "Bend, Stretch"
  else:
    fields = "Theta1, Theta2"
  rows = np.array([data[:, 0], data[:, 1]])
  filename = str(path)
  np.savetxt(
    filename,
    np.transpose(rows),
    fmt='%.2f',
    delimiter=',',
    header=fields
  )


epoch = datetime.utcfromtimestamp(0)


def unix_time_millis(dt):
  return (dt - epoch).total_seconds() * 1000.0


def main(exps=[8]):
  directories = get_sub_directories()

  X = []
  Y = []

  if len(exps) > 0:
    print 'len greater than 0'
    for i in range(len(exps)):
      exp_id = exps[i]
      print(exp_id)
      final_mat = manipcsv(exp_id)
      exp = Experiment(final_mat, exp_id)

      x = exp._inputs
      y = exp._truths

      plt.plot(x[:, 0], 'r')
      plt.plot(x[:, 1], 'b')
      plt.plot(y[:, 1], 'g')
      plt.legend()
      plt.show()

      delay = max(
        lag_finder(y[:,
                     0],
                   x[:,
                     0],
                   1),
        lag_finder(y[:,
                     1],
                   x[:,
                     0],
                   1)
      )
      x_adj = x[:-delay, :]
      for i in range(len(x_adj)):
        x_adj[i, :] = x[i + delay, :]
      y_adj = y[:-delay, :]

      flex_csv_path = str(directories[exp_id]) + '/Sync_flex.csv'
      take_csv_path = str(directories[exp_id]
                          ) + '/Sync_Take_{}_thetas.csv'.format(exp_id)

      exportcsv(x_adj, 'x', flex_csv_path)
      exportcsv(y_adj, 'y', take_csv_path)

  else:
    exps = [4, 5, 6, 7, 8, 9]
    # exps = [7, 8, 9]
    for i in range(len(exps)):
      exp_id = exps[i]
      print(exp_id)
      final_mat = manipcsv(exp_id)
      exp = Experiment(final_mat, exp_id)

      x = exp._inputs
      y = exp._truths

      delay = max(
        lag_finder(y[:,
                     0],
                   x[:,
                     0],
                   1),
        lag_finder(y[:,
                     1],
                   x[:,
                     0],
                   1)
      )
      x_adj = x[:-delay, :]
      for i in range(len(x_adj)):
        x_adj[i, :] = x[i + delay, :]
      y_adj = y[:-delay, :]

      # try:
      #   X = np.append(X, x_adj, axis=0)
      #   Y = np.append(Y, y_adj, axis=0)
      # except ValueError:
      #   X = x_adj
      #   Y = y_adj

      plt.plot(x_adj[:, 0], 'r')
      plt.plot(x_adj[:, 0], 'b')
      plt.plot(y_adj[:, 1], 'g')
      plt.legend()
      plt.show()

      np.save(
        root_path + '/scripts/data/inputs{}_adj.npy'.format(exp_id),
        x_adj
      )
      np.save(
        root_path + '/scripts/data/truths{}_adj.npy'.format(exp_id),
        y_adj
      )

    return


if __name__ == '__main__':
  # exp_number = 7
  if len(sys.argv) > 0:
    exp_number = int(sys.argv[1])
  pprint(main())
