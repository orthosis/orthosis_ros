#!/usr/bin/python

import numpy as np
import time
import os
import subprocess
import rospy
import rosparam
from datetime import datetime
import csv
import sys
from pathlib import Path

from os.path import expanduser

from bend_sensor_msg.msg import bend_sensor
from rosgraph_msgs.msg import Clock
from std_msgs.msg import Float32, String

epoch = datetime.fromtimestamp(0)


def unix_time_millis(dt):
  return int((dt - epoch).total_seconds())


def main():
  rospy.init_node('clock')
  rospy.set_param('/use_sim_time', True)
  clock_pub = rospy.Publisher('/clock', Clock, queue_size=1)
  # clock = rospy.Time()
  tm = Clock()

  while not rospy.is_shutdown():
    tm.clock.set(
      unix_time_millis(datetime.now()),
      datetime.now().microsecond * 1000
    )
    clock_pub.publish(tm)


if __name__ == '__main__':
  main()