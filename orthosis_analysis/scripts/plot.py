import matplotlib.pyplot as plt
import numpy as np
from sklearn import preprocessing
import urllib
import math
import csv_parser
import scipy
from scipy import signal


def lag_finder(y1, y2, sr):
  n = len(y1)

  corr = signal.correlate(
    y2,
    y1,
    mode='same'
  ) / np.sqrt(
    signal.correlate(y1,
                     y1,
                     mode='same')[int(n / 2)] *
    signal.correlate(y2,
                     y2,
                     mode='same')[int(n / 2)]
  )

  delay_arr = np.linspace(-0.5 * n / sr, 0.5 * n / sr, n)
  delay = delay_arr[np.argmax(corr)]

  return int(delay)


def lag_find(a, b):
  af = scipy.fft(a)
  bf = scipy.fft(b)
  c = scipy.ifft(af * scipy.conj(bf))

  time_shift = np.argmax(signal.correlate(a, b))

  return time_shift
  #return np.roll(b, shift=int(np.ceil(time_shift)))


scaler = preprocessing.MinMaxScaler()

x4 = np.load('inputs4.npy')
y4 = np.load('truths4.npy')

x5 = np.load('inputs5.npy')
y5 = np.load('truths5.npy')

x6 = np.load('inputs6.npy')
y6 = np.load('truths6.npy')

x7 = np.load('inputs7.npy')
y7 = np.load('truths7.npy')

x8 = np.load('inputs8.npy')
y8 = np.load('truths8.npy')

x9 = np.load('inputs9.npy')
y9 = np.load('truths9.npy')
'''
_,b7,s7 = np.loadtxt('/media/raunak/OS/Dynamics project/orthosis_ros-master-orthosis_analysis/orthosis_analysis/logs/log_2019_11_25:23_50_25/flex.csv', delimiter=',', unpack=True)
t17,t27 = np.loadtxt('/media/raunak/OS/Dynamics project/orthosis_ros-master-orthosis_analysis/orthosis_analysis/logs/log_2019_11_25:23_50_25/Take_7_thetas.csv', delimiter=',', unpack=True, usecols=(1,2),skiprows=1)
plt.plot(b7-np.amin(b7),'r')
plt.plot(t17+t27,'b')
plt.plot(s7,'g')
plt.suptitle('Take 4 red-bend, green-stretch, blue-theta1+theta2')
plt.show()
'''
X = np.append(x4, x5, axis=0)
y = np.append(y4, y5, axis=0)

X = np.append(X, x6, axis=0)
y = np.append(y, y6, axis=0)

X = np.append(X, x7, axis=0)
y = np.append(y, y7, axis=0)

X = np.append(X, x8, axis=0)
y = np.append(y, y8, axis=0)

X = np.append(X, x9, axis=0)
y = np.append(y, y9, axis=0)

print(len(X))

np.save('inputs.npy', X)
np.save('truths.npy', y)

x = np.load('inputs.npy')
y = np.load('truths.npy')
y_pred = np.load('predict.npy')
scaler.fit(y)
y_pred_sc = scaler.inverse_transform(y_pred)

#if np.amax(y7[:-300,0])>np.amax(y7[:-300,1]): delay = np.argmax(x7[:,0])-np.argmax(y7[:-300,0])
#else: delay = np.argmax(x7[:,0])-np.argmax(y7[:-300,1])

delay = max(
  lag_finder(y4[:,
                0],
             x4[:,
                0],
             1),
  lag_finder(y4[:,
                1],
             x4[:,
                0],
             1)
)
x4_adj = x4[:-delay, :]
for i in range(len(x4_adj)):
  x4_adj[i, :] = x4[i + delay, :]
y4_adj = y4[:-delay, :]

plt.plot(x4_adj[:, 0] - np.amin(x4_adj[:, 0]), 'r')
plt.plot(y4_adj[:, 0], 'b')
plt.plot(y4_adj[:, 1], 'g')
plt.plot(x4_adj[:, 1], 'k')
plt.suptitle('Take 4 red-bend, green-stretch, blue-theta1+theta2')

plt.show()
print delay
np.save('inputs4_adj.npy', x4_adj)
np.save('truths4_adj.npy', y4_adj)

#if np.amax(y8[:-1000,0])>np.amax(y8[:-1000,1]): delay = np.argmax(x8[:,0])-np.argmax(y8[:-1000,0])
#else: delay = np.argmax(x8[:,0])-np.argmax(y8[:-1000,1])
delay = max(
  lag_finder(y5[:,
                0],
             x5[:,
                0],
             1),
  lag_finder(y5[:,
                1],
             x5[:,
                0],
             1)
)
x5_adj = x5[:-delay, :]
for i in range(len(x5_adj)):
  x5_adj[i, :] = x5[i + delay, :]

y5_adj = y5[:-delay, :]

plt.plot(x5_adj[:, 0] - np.amin(x5_adj[:, 0]), 'r')
plt.plot(y5_adj[:, 0], 'b')
plt.plot(y5_adj[:, 1], 'g')
plt.plot(x5_adj[:, 1], 'k')
plt.suptitle('Take 5 red-bend, green-stretch, blue-theta1+theta2')
plt.show()
print delay
np.save('inputs5_adj.npy', x5_adj)
np.save('truths5_adj.npy', y5_adj)

#if np.amax(y9[:-300,0])>np.amax(y9[:-300,1]): delay = np.argmax(x9[:,0])-np.argmax(y9[:-300,0])
#else: delay = np.argmax(x9[:,0])-np.argmax(y9[:-300,1])
delay = max(
  lag_finder(y6[:,
                0],
             x6[:,
                0],
             1),
  lag_finder(y6[:,
                1],
             x6[:,
                0],
             1)
)
x6_adj = x6[:-delay, :]
for i in range(len(x6_adj)):
  x6_adj[i, :] = x6[i + delay, :]

y6_adj = y6[:-delay, :]

plt.plot(x6_adj[:, 0] - np.amin(x6_adj[:, 0]), 'r')
plt.plot(y6_adj[:, 0], 'b')
plt.plot(y6_adj[:, 1], 'g')
plt.plot(x6_adj[:, 1], 'k')
plt.suptitle('Take 6 red-bend, green-stretch, blue-theta1+theta2')
plt.show()
print delay
np.save('inputs6_adj.npy', x6_adj)
np.save('truths6_adj.npy', y6_adj)

delay = max(
  lag_finder(y7[:,
                0],
             x7[:,
                0],
             1),
  lag_finder(y7[:,
                1],
             x7[:,
                0],
             1)
)
x7_adj = x7[:-delay, :]
for i in range(len(x7_adj)):
  x7_adj[i, :] = x7[i + delay, :]
y7_adj = y7[:-delay, :]

plt.plot(x7_adj[:, 0], 'r')
plt.plot(y7[:-delay, 0], 'b')
plt.plot(y7[:-delay, 1], 'g')
plt.plot(x7_adj[:, 1], 'k')
plt.suptitle('Take 7 red-bend, green-stretch, blue-theta1+theta2')
plt.show()

np.save('inputs7_adj.npy', x7_adj)
np.save('truths7_adj.npy', y7_adj)

#if np.amax(y8[:-1000,0])>np.amax(y8[:-1000,1]): delay = np.argmax(x8[:,0])-np.argmax(y8[:-1000,0])
#else: delay = np.argmax(x8[:,0])-np.argmax(y8[:-1000,1])
delay = max(
  lag_finder(y8[:,
                0],
             x8[:,
                0],
             1),
  lag_finder(y8[:,
                1],
             x8[:,
                0],
             1)
)
x8_adj = x8[:-delay, :]
for i in range(len(x8_adj)):
  x8_adj[i, :] = x8[i + delay, :]
y8_adj = y8[:-delay, :]

plt.plot(x8_adj[:, 0], 'r')
plt.plot(y8[:-delay, 0], 'b')
plt.plot(y8[:-delay, 1], 'g')
plt.plot(x8_adj[:, 1], 'k')
plt.suptitle('Take 8 red-bend, green-stretch, blue-theta1+theta2')
plt.show()

np.save('inputs8_adj.npy', x8_adj)
np.save('truths8_adj.npy', y8_adj)

#if np.amax(y9[:-300,0])>np.amax(y9[:-300,1]): delay = np.argmax(x9[:,0])-np.argmax(y9[:-300,0])
#else: delay = np.argmax(x9[:,0])-np.argmax(y9[:-300,1])
delay = max(
  lag_finder(y9[:,
                0],
             x9[:,
                0],
             1),
  lag_finder(y9[:,
                1],
             x9[:,
                0],
             1)
)
x9_adj = x9[:-delay, :]
for i in range(len(x9_adj)):
  x9_adj[i, :] = x9[i + delay, :]
y9_adj = y9[:-delay, :]

plt.plot(x9_adj[:, 0], 'r')
plt.plot(y9[:-delay, 0], 'b')
plt.plot(y9[:-delay, 1], 'g')
plt.plot(x9_adj[:, 1], 'k')
plt.suptitle('Take 9 red-bend, green-stretch, blue-theta1+theta2')

plt.show()

np.save('inputs9_adj.npy', x9_adj)
np.save('truths9_adj.npy', y9_adj)

# first neural network with keras tutorial
# split into input (X) and output (y) variable
'''


plt.plot(x[:,0]-np.amin(x[:,0]),'r')
plt.plot(y[:,0]+y[:,1],'b')
plt.plot(x[:,1],'g')
plt.suptitle('Just about all the data!')


X = np.append(x7_adj,x8_adj,axis=0)
y = np.append(y7_adj,y8_adj,axis=0)

X = np.append(X,x9_adj,axis=0)
y = np.append(y,y9_adj,axis=0)

print(len(X))

np.save('inputs_adj.npy',X)
np.save('truths_adj.npy',y)
#plt.show()

#plt.plot(y_pred_sc[:,0],'g')


plt.plot(y[:,1],'b')
plt.plot(y_pred_sc[:,1],'g')

plt.show()

scaler2 = preprocessing.StandardScaler()
ys = scaler2.fit_transform(y)
yi = scaler2.inverse_transform(ys)
'''
