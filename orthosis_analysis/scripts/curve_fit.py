import numpy as np
import urllib
import math
import csv_parser
import keras
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.metrics import accuracy_score
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LeakyReLU
# from keras.layers.advanced_activations import LeakyReLU, PReLU
from keras.callbacks import callbacks
from keras.models import model_from_json
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
import os.path
from scipy import interp, arange, exp
import csv
from scipy import fftpack
#import ruamel.yaml

root_path = '/home/dhruv/catkin_ws/src/orthosis_ros/orthosis_analysis/scripts'


def scale_sample():
  scaler = preprocessing.MinMaxScaler()
  scaler = scaler.fit(X)
  return scaler


def rescale_angles():
  scaler = preprocessing.MinMaxScaler()
  scaler = scaler.fit(y)
  return scaler


def extrapolate(x1, y1, x2, y2, x):
  y = (y1 + (x-x1) / (x2-x1) * (y2-y1))

  return y


def butter_lowpass_filter(data, cutoff, fs, order=5):
  nyq = 0.5 * fs
  normal_cutoff = cutoff / nyq
  b, a = butter(order, normal_cutoff, btype='low', analog=False)
  y = lfilter(b, a, data)
  return y


def keras_model():
  # define the keras model

  model = Sequential()
  model.add(Dense(100, input_dim=2, activation='linear'))
  model.add(Dense(512))
  model.add(LeakyReLU(alpha=.01))
  model.add(Dense(512))
  model.add(LeakyReLU(alpha=.01))
  # model.add(Dense(32))
  # model.add(LeakyReLU(alpha=.05))
  # model.add(Dense(16))
  # model.add(LeakyReLU(alpha=.1))
  model.add(Dense(2))

  return model


# Split data into training and test sets

# load the CSV file as a numpy matrix
global X
global y

if os.path.isfile(root_path + '/data/inputs_adj1.npy'
                  ) and os.path.isfile(root_path + '/data/truths_adj.npy'):
  print('found!!!')
  X = np.load(root_path + '/data/inputs_adj.npy')
  y = np.load(root_path + '/data/truths_adj.npy')

else:
  # load inouts and truths of takes 4-9
  # inputs = X[:,0] (Bend values) and X[:,1] (Stretch values)
  # truths = y[:,0] (Theta 1) and y[:,1] (Theta 2)
  # read from adj files - contain adjusted data sets...

  x4 = np.load(root_path + '/data/inputs4_adj.npy')
  y4 = np.load(root_path + '/data/truths4_adj.npy')

  x5 = np.load(root_path + '/data/inputs5_adj.npy')
  y5 = np.load(root_path + '/data/truths5_adj.npy')

  x6 = np.load(root_path + '/data/inputs6_adj.npy')
  y6 = np.load(root_path + '/data/truths6_adj.npy')

  x7 = np.load(root_path + '/data/inputs7_adj.npy')
  y7 = np.load(root_path + '/data/truths7_adj.npy')

  x8 = np.load(root_path + '/data/inputs8_adj.npy')
  y8 = np.load(root_path + '/data/truths8_adj.npy')

  x9 = np.load(root_path + '/data/inputs9_adj.npy')
  y9 = np.load(root_path + '/data/truths9_adj.npy')

  # Only considering take 7,8,9. More can be considered...
  # X = np.append(x7, x8, axis=0)
  # y = np.append(y7, y8, axis=0)

  X = np.append(x7, x9, axis=0)
  y = np.append(y7, y9, axis=0)

  #np.save('inputs.npy',X)
  #np.save('truths.npy',y)

# limit for rate of change of theta 1 and theta 2
lim1 = np.max(map(abs, [t - s for s, t in zip(y[:, 0], y[1:, 0])]))
lim2 = np.max(map(abs, [t - s for s, t in zip(y[:, 1], y[1:, 1])]))
X_train, X_test, y_train, y_test = train_test_split(X, y,test_size = 0.15, random_state = 0)
print(len(X))

# Scale feature data
scaler = preprocessing.MinMaxScaler()
scaler.fit(X)
X_train_scaled = scaler.transform(X_train)
X_test_scaled = scaler.transform(X_test)

print scaler

# One hot encode target values
scaler.fit(y)
y_train_hot = scaler.transform(y_train)
y_test_hot = scaler.transform(y_test)

# Make Keras model or load stored model
retrain = False
if os.path.isfile(root_path + '/model/model.json') and not retrain:
  # load json and create model
  json_file = open(root_path + '/model/model.json', 'r')
  loaded_model_json = json_file.read()
  json_file.close()
  model = model_from_json(loaded_model_json)
  # load weights into new model
  model.load_weights(root_path + '/model/model.h5')

  model.compile(
    loss='mean_absolute_error',
    optimizer='adam',
    metrics=['mean_absolute_error',
             'accuracy']
  )
  _, mse, accuracy = model.evaluate(X_test_scaled, y_test_hot, verbose=0)
  print('Accuracy: %.2f' % (accuracy*100), mse)

else:
  model = keras_model()
  print('Created Model...')
  callbacks = [
    callbacks.EarlyStopping(
      monitor='loss',
      min_delta=0.0001,
      patience=100,
      verbose=0,
      mode='auto'
    )
  ]

  model.compile(
    loss='mean_absolute_error',
    optimizer='adam',
    metrics=['mean_absolute_error',
             'accuracy']
  )
  print('Compiled Model...')

  # fit the keras model on the dataset
  model.fit(
    X_train_scaled,
    y_train_hot,
    epochs=2000,
    batch_size=54,
    verbose=1,
    callbacks=callbacks
  )
  print "\n\n\n\n"

  print('Model Fit...')

  # evaluate the keras model
  _, mse, accuracy = model.evaluate(X_test_scaled, y_test_hot, verbose=0)
  print('Accuracy: %.2f' % (accuracy*100), mse)

  # serialize model to JSON
  model_json = model.to_json()
  with open(root_path + '/model/model.json', 'w') as json_file:
    json_file.write(model_json)
  # serialize weights to HDF5
  model.save_weights(root_path + '/model/model.h5')

  # scalar_dict = {
  # 	"scalar": {
  # 		"x": scaler.fit(X)
  # 	}
  # }
  # with open(root_path + '/model/model.yaml', 'w+') as yaml_file:
  # 	yaml_file.write()

  print("Saved model to disk")

# Testing

# Random testing
# idx = np.random.randint(len(X_train_scaled),size=12000)
# Xi = np.zeros((12000,2))
# yi = np.zeros((12000,2))
# err = np.zeros((len(X),2))
# err1 = np.zeros((len(X),2))

# for i in range(len(idx)):
# 	Xi[i,:] = X_train_scaled[idx[i],:]
# 	yi[i,:] = y_train_hot[idx[i],:]

# for i in range(len(yi_pred1)-1):
# 	if yi_pred_corr[i,0]-yi_pred_corr[i+1,0]>.5:#(0.1*np.mean(map(abs,[t - s for s, t in zip(X_test[:,0], X_test[1:,0])]))):
# 		yi_pred_corr[i+1,0]=yi_pred_corr[i,0]
# 	else:
# 		yi_pred_corr[i,0] = yi_pred1[i,0]

# 	if yi_pred_corr[i,1]-yi_pred_corr[i+1,1]>.5:#(0.1*np.mean(map(abs,[t - s for s, t in zip(X_test[:,0], X_test[1:,0])]))):
# 		yi_pred_corr[i+1,1]=yi_pred_corr[i,1]
# 	else:
# 		yi_pred_corr[i,1] = yi_pred1[i,1]

# Testing with entire dataset

y_pred_Sc = model.predict(scaler.fit_transform(X))
scaler.fit(y)
y_pred = scaler.inverse_transform(y_pred_Sc)
y_pred_corr = np.zeros(np.shape(y_pred))  #corrected predictions

# Low pass Filter requirements.
# order = 1
# fs = 100.0       # sample rate, Hz
# cutoff = 33  # desired cutoff frequency of the filter, Hz
# y_pred_corr = butter_lowpass_filter(y_pred, cutoff, fs, order)

y_pred_corr0 = np.copy(y_pred[:, 0])
y_pred_corr1 = np.copy(y_pred[:, 1])

y_pred_corr0_temp = np.copy(y_pred_corr0)
y_pred_corr1_temp = np.copy(y_pred_corr1)

# Extrapolation of spurious values
for i in range(2, len(y_pred_corr1)):
  if (
    y_pred_corr0[i] - y_pred_corr0[i - 1]
  ) > 5:  #(0.1*np.mean(map(abs,[t - s for s, t in zip(X_test[:,0], X_test[1:,0])]))):
    y_pred_corr0_temp[i] = extrapolate(
      i - 2,
      y_pred_corr0[i - 2],
      i - 1,
      y_pred_corr0[i - 1],
      i
    )

  if (
    y_pred_corr1[i] - y_pred_corr1[i - 1]
  ) > 10:  #(0.1*np.mean(map(abs,[t - s for s, t in zip(X_test[:,0], X_test[1:,0])]))):
    y_pred_corr1_temp[i] = extrapolate(
      i - 2,
      y_pred_corr1[i - 2],
      i - 1,
      y_pred_corr1[i - 1],
      i
    )

y_pred_corr0 = y_pred_corr0_temp
y_pred_corr1 = y_pred_corr1_temp

# Moving average window convolutiom
N = 2
count = 0
y_pred_corr0 = np.convolve(y_pred_corr0[:], np.ones((N,)) / N, mode='valid')
y_pred_corr1 = np.convolve(y_pred_corr1[:], np.ones((N,)) / N, mode='valid')

length = len(y_pred_corr1)
err = np.zeros((len(X), 2))
err1 = np.zeros((len(X), 2))

# Error in corrected predictions over dataset
for i in range(length):
  err1[i, 0] = abs(y[i, 0] - y_pred_corr0[i])
  err1[i, 1] = abs(y[i, 1] - y_pred_corr1[i])

# Error in predictions over dataset
for i in range(len(X)):
  err[i, :] = abs(y[i, :] - y_pred[i, :])

print ""
print("Original Prediction minimum, maximum and mean error")
print "MCP Joint"
print(np.amin(err[:, 0]), np.amax(err[:, 0]), np.mean(err[:, 0]))
print "PIP Joint"
print(np.amin(err[:, 1]), np.amax(err[:, 1]), np.mean(err[:, 1]))

# print("Corrected Prediction minimum, maximum and mean error")
# print(np.amin(err1[:, 0]), np.amax(err1[:, 0]), np.mean(err1[:, 0]))
# print(np.amin(err1[:, 1]), np.amax(err1[:, 1]), np.mean(err1[:, 1]))

# Save predicted and original data in a csv file
# field names
fields = "Bend, Stretch, theta1, theta1_predicted, error1, theta2, theta2_predicted, error2"
# data rows of csv file
rows = np.array(
  [
    X[:,
      0],
    X[:,
      1],
    y[:,
      0],
    y_pred[:,
           0],
    abs(y[:,
          0] - y_pred[:,
                      0]),
    y[:,
      1],
    y_pred[:,
           1],
    abs(y[:,
          1] - y_pred[:,
                      1])
  ]
)  #, lim11, lim22])
# print np.shape(np.transpose(rows))
# name of csv file
filename = root_path + '/output/predicted_values.csv'
np.savetxt(
  filename,
  np.transpose(rows),
  fmt='%.2f',
  delimiter=',',
  header=fields
)

# Plotting data
# plot input bend values
plt.subplot(211)
bend, = plt.plot(X[:, 0])
bend.set_label('Bend Values')

stretch, = plt.plot(X[:, 1])
stretch.set_label('Stretch Values')

# plot true theta1
mcp, = plt.plot(y[:, 0])
mcp.set_label('MCP Truth')

# plot predicted theta1
mcp_p, = plt.plot(y_pred[:, 0])
mcp_p.set_label('MCP Predicted')

# plot corrected predicted theta1
# plt.plot(y_pred_corr0)
plt.legend()
# plot true theta2
plt.subplot(212)
bend, = plt.plot(X[:, 0])
bend.set_label('Bend Values')

stretch, = plt.plot(X[:, 1])
stretch.set_label('Stretch Values')

# plot true theta1
pip, = plt.plot(y[:, 1])
pip.set_label('PIP Truth')

# plot predicted theta1
pip_p, = plt.plot(y_pred[:, 1])
pip_p.set_label('PIP Predicted')
# plot corrected predicted theta2
# plt.plot(y_pred_corr1)

# plot input stretch values
# plt.plot(X[:, 1])
plt.legend()
plt.show()