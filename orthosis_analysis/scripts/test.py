#!/usr/bin/python

from plotter import get_latest_log_folder
import os
from os.path import expanduser


def main():
  get_latest_log_folder()
  return


if __name__ == "__main__":
  main()