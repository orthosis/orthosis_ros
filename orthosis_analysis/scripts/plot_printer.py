# !/usr/bin/python

import numpy as np
import csv_parser as csv
import fitting as ft
import matplotlib.pyplot as plt
from datetime import datetime
from pprint import pprint
import os
import sys


def list_experiments():

  root_path = csv.get_root_path() + '/logs'

  experiments = csv.get_sub_directories()

  EXP = []
  for k, v in experiments.items():
    str_exp = v.split('/')
    exp = str_exp[len(str_exp) - 1]
    exp = exp.split('_')
    del exp[0]
    j = 0
    for i, s in enumerate(exp):
      exp[i] = s.split(':')

    _exp = []
    for s1 in exp:
      for s2 in s1:
        _exp.append(s2)

    EXP.append(_exp)

  EXP.reverse()

  dt = [
    '{}/{}/{}:{}-{}-{}'.format(e[2],
                               e[1],
                               e[0],
                               e[3],
                               e[4],
                               e[5]) for e in EXP
  ]

  paths = experiments.values()
  paths.reverse()

  for i, (k, v) in enumerate(zip(dt, paths)):
    # d = {i, v}
    experiments[i] = v

  pprint(experiments)

  return experiments


def select_experiment(experiments):

  id = int(raw_input('Select exp id:\t'))

  if id < 0 and id >= len(experiments.values()):
    id = 0

  return (id, experiments[id])


def main():

  experiments = list_experiments()

  exp = select_experiment(experiments)

  bend_flag = False
  motor_flag = False
  bend = []
  stretch = []
  flex = []
  motor_measured = []
  motor_desired = []
  if os.path.exists(exp[1] + '/flex.npy'):
    flex = np.load(exp[1] + '/flex.npy')
    for i, row in enumerate(flex):
      stretch.append(row[1])
      bend.append(row[2])

    bend_flag = True

  if os.path.exists(exp[1] + '/motor_measured.npy'
                    ) and os.path.exists(exp[1] + '/motor_desired.npy'):
    motor_measured_ds = np.load(exp[1] + '/motor_measured.npy')
    # motor_desired = np.load(exp[1] + '/motor_desired.npy')
    for i, row in enumerate(motor_measured_ds):
      if len(row) == 3:
        motor_measured.append(row[1])
        motor_desired.append(row[2])
      # if len(row) == 2:
      #   if i < len(motor_desired):
      #     motor_desired[i] = row[1]
      #   else:
      #     motor_desired.append(row[1])

    motor_flag = True

  if motor_flag:
    mm, = plt.plot([i for i in range(len(motor_measured))], motor_measured)

    md, = plt.plot([i for i in range(len(motor_measured))], motor_desired)

    mm.set_label('Measured Force')
    md.set_label('Measured Torque')

    plt.show()

  return


if __name__ == '__main__':
  main()
