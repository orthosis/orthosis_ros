#!/usr/bin/env python
""" Plot flex sensor data """

# import rosbag
import numpy as np

import matplotlib.pyplot as plt
from datetime import datetime, timedelta

from plotter import Plotter, load_data, get_latest_log_folder


class BendPlotter(Plotter):
  """ Bend Value Plotter """

  def __init__(self, logs_folder, filename="bend.npy"):
    super(BendPlotter,
          self).__init__(
            name="Bend",
            data=load_data(logs_folder,
                           filename)
          )
    self._filename = filename
    return


class StretchPlotter(Plotter):
  """ Bend Value Plotter """

  def __init__(self, logs_folder, filename="stretch.npy"):
    super(StretchPlotter,
          self).__init__(
            name="Stretch",
            data=load_data(logs_folder,
                           filename)
          )
    self._filename = filename
    return


class FlexPlotter(Plotter):
  """ Plot Flex data """

  def __init__(self, logs_folder, filename="flex.npy"):
    super(FlexPlotter,
          self).__init__(
            name="Flex",
            data=load_data(logs_folder,
                           filename)
          )
    self._filename = filename

    return


def rev_time(t=1574134669752):
  epoch = datetime(1601, 1, 1)
  cookie_microseconds_since_epoch = 13022344559000000
  cookie_datetime = epoch + timedelta(
    microseconds=cookie_microseconds_since_epoch
  )
  print str(cookie_datetime)


def main():
  """ Main """
  # dir_latest_logs = get_latest_log_folder(
  #   # directory='~/catkin_ws/src/orthosis_ros/orthosis_analysis/logs'
  # )
  # print dir_latest_logs
  rev_time()

  flex_plotter = FlexPlotter(
    # dir_latest_logs
    '/home/dhruv/catkin_ws/src/orthosis_ros/orthosis_analysis/logs/log_2019_12_15:11_43_32'
  )
  flex_plotter.plot()
  plt.show()

  return


if __name__ == '__main__':
  main()
