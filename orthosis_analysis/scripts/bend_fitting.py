#!/usr/bin/python
import csv_parser
from pprint import pprint
import numpy as np
from numpy import loadtxt
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline


# Activation function
def sigmoid(t):
  return 1 / (1 + np.exp(-t))


# Derivative of sigmoid
def sigmoid_derivative(p):
  return p * (1-p)


class Experiment(object):

  def __init__(self, exp_data, exp_id=0):
    super(Experiment, self).__init__()
    self._len = len(exp_data)
    self._indexes = exp_data[0:self._len][0]
    self._stamps = exp_data[0:self._len][1]
    self._truths = np.ndarray((self._len, 2))
    self._inputs = np.ndarray((self._len, 2))
    self._exp_id = exp_id
    self._exp_data = exp_data

    for i in range(self._len):
      self._truths[i][0] = exp_data[i][2]
      self._truths[i][1] = exp_data[i][3]
      self._inputs[i][0] = exp_data[i][4]
      self._inputs[i][1] = exp_data[i][5]

  def __repr__(self):
    super(Experiment, self).__repr__()
    return {
      'exp_id': self._exp_id,
      'len': self._len,
      'indexes': self._indexes,
      'stamps': self._stamps,
      'truths': [_truths for _truths in self._truths],
      'inputs': [_inputs for _inputs in self._inputs]
    }

  def __str__(self):
    super(Experiment, self).__str__()
    return 'Experiment(exp_id=' + str(self._exp_id) + ')'

  def to_string(self):
    return pprint(self._exp_data)


class NeuralNetwork(Experiment):

  def __init__(self, exp_data, exp_id=0):
    super(NeuralNetwork, self).__init__(exp_data, exp_id=exp_id)
    self.input = self._inputs
    self.y = self._truths
    self.output = np.zeros(self.y.shape)
    # considering we have 4 nodes in the hidden layer
    self.weights1 = np.random.rand(self.input.shape[1], 4)
    self.weights2 = np.random.rand(4, 1)

  def feedforward(self):
    self.layer1 = sigmoid(np.dot(self.input, self.weights1))
    self.layer2 = sigmoid(np.dot(self.layer1, self.weights2))
    return self.layer2

  def backprop(self):
    d_weights2 = np.dot(
      self.layer1.T,
      2 * (self.y - self.output) * sigmoid_derivative(self.output)
    )
    d_weights1 = np.dot(
      self.input.T,
      np.dot(
        2 * (self.y - self.output) * sigmoid_derivative(self.output),
        self.weights2.T
      ) * sigmoid_derivative(self.layer1)
    )

  def train(self):
    self.output = self.feedforward()
    self.backprop()


def main():
  exp_id = 2
  final_mat = csv_parser.main(exp_id)
  exp = NeuralNetwork(final_mat, exp_id)

  for i in range(1500):
    if i % 100 == 0:
      print("for iteration # " + str(i) + "\n")
      print("Input : \n" + str(exp.input))
      print("Actual Output: \n" + str(exp.y))
      print("Predicted Output: \n" + str(exp.feedforward()))
      print(
        "Loss: \n" + str(np.mean(np.square(exp.y - exp.feedforward())))
      )  # mean sum squared loss
      print("\n")

    exp.train()


def run():
  exp_id = 2
  final_mat = csv_parser.main(exp_id)
  exp = Experiment(final_mat, exp_id)
  # first neural network with keras tutorial
  # split into input (X) and output (y) variables
  X = exp._inputs
  y = exp._truths
  # inputs = [[0., 0.] for i in range(exp._inputs.shape[0])]
  # for i, row in enumerate(exp._inputs):
  #   for j, ele in enumerate(row):
  #     inputs[i][j] = ele

  # print inputs

  # truths = [[0., 0.] for i in range(exp._truths.shape[0])]
  # for i, row in enumerate(exp._truths):
  #   for j, ele in enumerate(row):
  #     truths[i][j] = ele

  # print truths

  # X = inputs
  # y = truths

  # fit the keras model on the dataset
  estimators = []
  estimators.append(('standardize', StandardScaler()))
  estimators.append(
    (
      'mlp',
      KerasRegressor(
        build_fn=wider_model,
        epochs=100,
        batch_size=5,
        verbose=0
      )
    )
  )
  pipeline = Pipeline(estimators)

  kfold = KFold(n_splits=10)
  results = cross_val_score(pipeline, X, y, cv=kfold)
  print("Wider: %.2f (%.2f) MSE" % (results.mean(), results.std()))


def wider_model():
  # define the keras model
  model = Sequential()
  model.add(Dense(3, input_dim=2, activation='sigmoid'))
  model.add(Dense(8, activation='relu'))
  model.add(Dense(2, activation='softmax'))
  # print model.summary()
  # compile the keras model
  model.compile(loss='mse', optimizer='rmsprop')  #, metrics=['accuracy']
  return model


if __name__ == '__main__':
  run()