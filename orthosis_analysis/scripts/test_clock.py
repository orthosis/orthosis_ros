#!/usr/bin/python

import rospy
import numpy as np


def main():

  rospy.init_node('time_test')
  while not rospy.is_shutdown():
    print(np.float64)(rospy.Time.now().to_nsec()) / 10**6

  return


if __name__ == '__main__':
  main()