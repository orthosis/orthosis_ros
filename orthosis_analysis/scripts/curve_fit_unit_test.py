#!/usr/bin/python

from fitting import predict_values, model_load, scale_sample, load_dataset
import numpy as np

import rospy
from bend_sensor_msg.msg import bend_sensor


def unit_test():

  x, y = load_dataset([9])

  bend, stretch, row = spam_values(x)

  model = model_load()
  sample = np.ndarray(shape=(1, 2), dtype=float)
  sample = np.array([[bend, stretch]])
  X = scale_sample().transform(sample)
  Y_predict = predict_values(X, model)

  print Y_predict
  print y[row]

  return


def spam_values(x):
  row = np.random.random_integers(0, np.shape(x)[0] - 1)

  bend = x[row][0]
  stretch = x[row][1]

  return bend, stretch, row


def ros_unit_test():

  rospy.init_node('unit_test_bend', anonymous=False)

  fingers = ['index', 'middle', 'ring', 'pinky']

  pubs_fingers = [
    rospy.Publisher(
      '/devices/{}/bend_sensor'.format(finger),
      bend_sensor,
      queue_size=10
    ) for finger in fingers
  ]

  rate = rospy.Rate(100)
  x, y = load_dataset([9])

  while not rospy.is_shutdown():
    for i, finger in enumerate(fingers):
      bend, stretch, row = spam_values(x)
      msg = bend_sensor()
      msg.chip_id.data = 0x12
      msg.header.frame_id = finger
      msg.header.stamp = rospy.Time.now()
      msg.bend.data = bend
      msg.stretch.data = stretch
      pubs_fingers[i].publish(msg)

    rate.sleep()

  return


if __name__ == "__main__":
  # unit_test()
  try:
    ros_unit_test()
  except rospy.ROSInterruptException:
    pass