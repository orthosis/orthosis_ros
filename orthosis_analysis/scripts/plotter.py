#!/usr/bin/python
""" Plot experiment logs """

import os
from os.path import expanduser

import numpy as np
import matplotlib.pyplot as plt
# import rospkg

# rospack = rospkg.RosPack()
# rospack.list()

# root_path = rospack.get_path('orthosis_analysis')
# base_path = root_path + '/logs/'


def get_latest_log_folder(directory=None):
  """ Get latest log folder """
  if directory is None:
    dir_logs = base_path  # expanduser("~") + "/.orthosis/logs/"
  else:
    dir_logs = directory
  directories = {d: dir_logs + d for d in os.listdir(dir_logs)}
  dirs = []
  for k, v in directories.items():
    k_list = k.split('_')
    k_ = ""
    for k_l in k_list:
      k_ += k_l
    d_ = {k_: v}
    directories.update(d_)
    del directories[k]
    dirs.append(k_)

  d_path = directories[max(directories.keys())]
  return d_path


def load_data(logs_folder, filename):
  """ Load data """
  return np.load("{}/{}".format(logs_folder, filename), allow_pickle=True)


class Plotter(object):
  """ Plotter to plot different sensory data  """

  def __init__(self, name, data):
    super(Plotter, self).__init__()
    self.name = name
    self.data = data
    return

  def plot(self):
    """ Plot data """

    self.time_vals = [self.data[i][0] for i in range(self.data.shape[0])]
    # print self.time_vals
    self.time = [i for i in range(self.data.shape[0])]
    self.bend = [self.data[i][1] for i in range(self.data.shape[0])]
    self.flex = [self.data[i][2] for i in range(self.data.shape[0])]

    plt.figure(self.name)
    plt.subplot(3, 1, 1)
    plt.plot(self.time, self.bend)
    plt.title('bend')
    plt.grid(True)
    plt.xlabel('Time')
    plt.xlabel('Bend in Rads')

    plt.subplot(3, 1, 2)
    plt.plot(self.time, self.flex)
    plt.title('flex')
    plt.grid(True)
    plt.xlabel('Time')
    plt.xlabel('Flex in mm')

    self.bend_degrees = self.bend
    plt.subplot(3, 1, 3)
    plt.plot(self.time, np.transpose(np.array([self.bend_degrees, self.flex])))
    plt.title('flex and bend')
    plt.grid(True)
    plt.xlabel('Time')
    plt.xlabel('Flex and Bend in mm/degrees')

    return
