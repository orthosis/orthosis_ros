#! /usr/bin/python
import numpy as np
import urllib
import math
import csv_parser
import keras
import keras.backend as k
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.metrics import accuracy_score
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LeakyReLU
from keras.callbacks import callbacks
from keras.models import model_from_json
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
import os.path
from scipy import interp, arange, exp
import csv
from scipy import fftpack
import tensorflow as tf

global X
global y

X = None
y = None


def get_root_scripts_path():
  return csv_parser.get_root_path() + '/scripts'


root_path = get_root_scripts_path()


def scale_sample():
  scaler = preprocessing.MinMaxScaler()
  scaler = scaler.fit(X)
  return scaler


def model_load():
  # with graph.as_default():
  json_file = open(root_path + '/model/model.json', 'r')
  loaded_model_json = json_file.read()
  json_file.close()
  model = model_from_json(loaded_model_json)
  model.load_weights(root_path + '/model/model.h5')
  model.compile(
    loss='mean_absolute_error',
    optimizer='adam',
    metrics=['mean_absolute_error',
             'accuracy']
  )

  # model._make_predict_function()

  # assert model.graph is graph

  return model


def predict_values(x_transformed, model):  # = scaler.fit_transform(X)
  scaler = preprocessing.MinMaxScaler()
  # y_pred = np.array([[0.0, 0.0]])
  # with graph.as_default():
  y_pred = model.predict(x_transformed)
  scaler.fit(y)
  y_pred = scaler.inverse_transform(y_pred)

  return y_pred


def keras_model():
  model = Sequential()

  model.add(Dense(100, input_dim=2, activation='linear'))
  model.add(Dense(512))
  model.add(LeakyReLU(alpha=.01))
  model.add(Dense(512))
  model.add(LeakyReLU(alpha=.01))
  # model.add(Dense(32))
  # model.add(LeakyReLU(alpha=.05))
  # model.add(Dense(16))
  # model.add(LeakyReLU(alpha=.1))
  model.add(Dense(2))

  return model


def load_dataset(exp_numbers=[7, 9]):
  global X
  global y

  if os.path.isfile(root_path + '/data/inputs_adj1.npy'
                    ) and os.path.isfile(root_path + '/data/truths_adj.npy'):
    print('found!!!')
    X = np.load(root_path + '/data/inputs_adj.npy')
    y = np.load(root_path + '/data/truths_adj.npy')

  else:

    rel_path = [
      [
        root_path + '/data/{}{}_adj.npy'.format(typ,
                                                exp)
        for typ in ['inputs',
                    'truths']
      ]
      for exp in exp_numbers
    ]

    for exp in rel_path:
      if X is not None:
        X = np.append(X, np.load(exp[0]), axis=0)
      else:
        X = np.load(exp[0])
      if y is not None:
        y = np.append(y, np.load(exp[1]), axis=0)
      else:
        y = np.load(exp[1])

  return X, y


def test_and_train(exp_numbers=[7, 9], retrain=True):
  load_dataset(exp_numbers)
  scaler = scale_sample()
  if retrain:
    # limit for rate of change of theta 1 and theta 2
    lim1 = np.max(map(abs, [t - s for s, t in zip(y[:, 0], y[1:, 0])]))
    lim2 = np.max(map(abs, [t - s for s, t in zip(y[:, 1], y[1:, 1])]))
    X_train, X_test, y_train, y_test = train_test_split(X, y,test_size = 0.15, random_state = 0)
    print(len(X))

    # Scale feature data
    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)

    print scaler

    # One hot encode target values
    scaler.fit(y)
    y_train_hot = scaler.transform(y_train)
    y_test_hot = scaler.transform(y_test)
  # else:

  # Make Keras model or load stored model
  if os.path.isfile(root_path + '/model/model.json') and not retrain:
    model = model_load()
    scaler.fit(y)
  else:
    model = keras_model()
    print('Created Model...')

    model.compile(
      loss='mean_absolute_error',
      optimizer='adam',
      metrics=['mean_absolute_error',
               'accuracy']
    )
    print('Compiled Model...')

    # fit the keras model on the dataset
    cbs = [
      callbacks.EarlyStopping(
        monitor='loss',
        min_delta=0.0001,
        patience=100,
        verbose=0,
        mode='auto'
      )
    ]
    model.fit(
      X_train_scaled,
      y_train_hot,
      epochs=2000,
      batch_size=54,
      verbose=1,
      callbacks=cbs
    )
    print('Model Fit...')

    model_json = model.to_json()
    with open(root_path + '/model/model.json', 'w') as json_file:
      json_file.write(model_json)

    model.save_weights(root_path + '/model/model.h5')

    _, mse, accuracy = model.evaluate(X_test_scaled, y_test_hot, verbose=0)
    print('Accuracy: %.2f' % (accuracy*100), mse)

  y_pred = predict_values(scaler.fit_transform(X), model)

  # plt.subplot(211)
  # plt.plot(X[:, 0])
  # plt.plot(y[:, 0])

  # plt.subplot(212)
  # plt.plot(X[:, 0])
  # plt.plot(y[:, 1])

  # Plotting data
  # plot input bend values
  plt.subplot(211)
  # bend, = plt.plot(X[:, 0])
  # bend.set_label('Bend Values')

  # stretch, = plt.plot(X[:, 1])
  # stretch.set_label('Stretch Values')

  # plot true theta1
  mcp, = plt.plot(y[:, 0])
  mcp.set_label('MCP Truth')

  mcp_p, = plt.plot(y_pred[:, 0])
  mcp_p.set_label('MCP Predicted')

  # plot predicted theta1
  # mcp_p, = plt.plot(y_pred[:, 0])
  # mcp_p.set_label('MCP Predicted')

  # plot corrected predicted theta1
  # plt.plot(y_pred_corr0)
  plt.legend(
    loc='upper center',
    bbox_to_anchor=(0.5,
                    -0.05),
    shadow=True,
    ncol=4
  )
  # plot true theta2
  plt.subplot(212)
  # bend, = plt.plot(X[:, 0])
  # bend.set_label('Bend Values')

  # stretch, = plt.plot(X[:, 1])
  # stretch.set_label('Stretch Values')

  # plot true theta1
  pip, = plt.plot(y[:, 1])
  pip.set_label('PIP Truth')

  pip_p, = plt.plot(y_pred[:, 1])
  pip_p.set_label('PIP Predicted')

  # plot predicted theta1

  # plot corrected predicted theta2
  # plt.plot(y_pred_corr1)

  # plot input stretch values
  # plt.plot(X[:, 1])
  plt.legend(
    loc='upper center',
    bbox_to_anchor=(0.5,
                    -0.05),
    shadow=True,
    ncol=4
  )
  plt.show()

  return


def main():
  """ Main """

  test_and_train(exp_numbers=[7, 9], retrain=False)

  return


if __name__ == "__main__":
  main()
