#!/usr/bin/python

import numpy as np
import time
import os
import subprocess
import rospy
import rosparam
import datetime
import csv
import sys
from pathlib import Path

from os.path import expanduser

from bend_sensor_msg.msg import bend_sensor
from motor_msg.msg import motor_desired, motor_measured
from rosgraph_msgs.msg import Clock
from std_msgs.msg import Float32, String
import rospkg

rospack = rospkg.RosPack()
rospack.list()

root_path = rospack.get_path('orthosis_analysis')

BUFFER_SIZE = int(1e6)
base_time = int(round(time.time() * 1000))


def verify_and_create_log_folder(directory=None):
  """ Verify and create folder """
  # Package folder
  if directory is None:
    directory = expanduser("~") + "/.orthosis"
  # Create package folder
  if not os.path.exists(directory):
    print("Creating folder {}".format(directory))
    os.makedirs(directory)
  # Create logs folder
  logs = "/logs"
  if not os.path.exists(directory + logs):
    print("Creating folder {}".format(directory + logs))
    os.makedirs(directory + logs)
  # Create experiment folder
  date = datetime.datetime.now()
  logdate = "/log_{}".format(date.strftime('%Y_%m_%d:%H_%M_%S'))
  if not os.path.exists(directory + logs + logdate):
    print("Creating folder {}".format(directory + logs + logdate))
    os.makedirs(directory + logs + logdate)
  else:
    raise Exception("Folder already exists")
  return directory + logs + logdate


class Log(object):
  """ Logged data """

  def __init__(self, filename, size, buffer_size=BUFFER_SIZE):
    super(Log, self).__init__()
    self.filename = filename
    self.buffer_size = buffer_size
    self.data = np.zeros([self.buffer_size, size])
    self.iteration = 0
    return

  def update(self, data):
    """ Update data """
    self.data[self.iteration, :] = data[:]
    self.iteration += 1
    return

  def save(self, fmt="%u,%1.8f,%1.8f"):
    """ Save data to file """
    print("Saving {}".format(self.filename))
    np.save(self.filename, self.data[:self.iteration])
    if not os.path.exists(self.filename + '.csv'):
      print("Writing CSV File")
      np.savetxt(
        fname=self.filename + '.csv',
        X=self.data[:self.iteration],
        # delimiter=",",
        fmt=fmt
      )
    return


class LogBendValues(Log):
  """ Log Bend values """

  def __init__(self, folder, size):
    super(LogBendValues, self).__init__(filename=folder + "/bend", size=size)
    self.iteration = 1
    return


class LogStretchValues(Log):
  """ Log Stretch values """

  def __init__(self, folder, size):
    super(LogStretchValues,
          self).__init__(
            filename=folder + "/stretch",
            size=size
          )
    self.iteration = 1
    return


class LogClock(Log):

  def __init__(self, folder, size=1):
    super(LogClock, self).__init__(filename=folder + "/clock", size=1)
    self.iteration = 1
    self.sub = rospy.Subscriber("/clock", Clock, self.callback)
    return

  def callback(self, msg):
    """ Flex Sensor CB """

    self.clock_time = int(
      msg.clock.secs * 1000 + int(float(msg.clock.nsecs) / 1000000)
    )
    return self.update(np.array([self.clock_time]))


class LogFlexValues(Log):
  """ Log bend and stretch values """

  def __init__(self, folder, size):
    super(LogFlexValues, self).__init__(filename=folder + "/flex", size=size)
    self.iteration = 1
    return

  def callback(self, msg):
    """ Flex Sensor CB """

    # print self.clock.clock_time
    return self.update(
      np.array(
        [
          (np.float64)(msg.header.stamp.to_nsec()) / 10**6,
          msg.bend.data,
          msg.stretch.data
        ]
      )
    )


class LogFlexSensor(object):
  """ Flex Sensor logger """

  def __init__(self, logs_folder):
    super(LogFlexSensor, self).__init__()
    size = 3
    # self.bend = LogBendValues(logs_folder, size)
    # self.stretch = LogStretchValues(logs_folder, size)
    self.flex = LogFlexValues(logs_folder, size)

    self.sub = rospy.Subscriber(
      "/devices/index/bend_sensor",
      bend_sensor,
      self.flex.callback
    )
    return

  # def callback(self, data):
  #   """ Callback """
  #   # self.bend.update(data.bend.data)
  #   # self.stretch.update(data.stretch.data)
  #   self.flex.update(data)
  #   return

  def save(self):
    """ Save data """
    # self.bend.save()
    # self.stretch.save()
    self.flex.save()

    return


class LogMotorMeasuredValues(Log):
  """ Log bend and stretch values """

  def __init__(self, folder, size):
    super(LogMotorMeasuredValues,
          self).__init__(
            filename=folder + "/motor_measured",
            size=size
          )
    self.iteration = 1
    return

  def callback(self, msg):
    """ Motor Sensor CB """

    # print self.clock.clock_time
    return self.update(
      np.array(
        [
          (np.float64)(msg.header.stamp.to_nsec()) / 10**6,
          msg.measured_force.data,
          msg.desired_force.data
        ]
      )
    )


class LogMotorDesiredValues(Log):
  """ Log bend and stretch values """

  def __init__(self, folder, size):
    super(LogMotorDesiredValues,
          self).__init__(
            filename=folder + "/motor_desired",
            size=size
          )
    self.iteration = 1
    return

  def callback(self, msg):
    """ Motor Sensor CB """

    # print self.clock.clock_time
    return self.update(
      np.array(
        [
          (np.float64)(msg.header.stamp.to_nsec()) / 10**6,
          msg.desired_force.data,
          msg.torque_constant.data
        ]
      )
    )


class LogMotorMeasuredSensor(object):
  """ Flex Sensor logger """

  def __init__(self, logs_folder):
    super(LogMotorMeasuredSensor, self).__init__()
    msize = 3
    self.motor_measured = LogMotorMeasuredValues(logs_folder, msize)

    self.sub = rospy.Subscriber(
      "/devices/index/motor_measured",
      motor_measured,
      self.motor_measured.callback
    )
    return

  def save(self):
    """ Save data """
    self.motor_measured.save(fmt="%f,%f,%f")

    return


class LogMotorDesiredSensor(object):
  """ Flex Sensor logger """

  def __init__(self, logs_folder):
    super(LogMotorDesiredSensor, self).__init__()
    dsize = 3
    self.motor_desired = LogMotorDesiredValues(logs_folder, dsize)

    self.sub = rospy.Subscriber(
      "/devices/index/motor_desired",
      motor_desired,
      self.motor_desired.callback
    )
    return

  def save(self):
    """ Save data """
    self.motor_desired.save(fmt="%f,%f,%f")

    return


class Loggers(object):
  """ Loggers """

  def __init__(self, logs_folder):
    super(Loggers, self).__init__()
    self.logs_folder = logs_folder
    self.flex_sensor = LogFlexSensor(logs_folder)
    self.motor_measured_sensor = LogMotorMeasuredSensor(logs_folder)
    self.motor_desired_sensor = LogMotorDesiredSensor(logs_folder)
    # self.clock = LogClock(logs_folder)

    # Rosbag logging
    topics = " ".join(["/devices/index/bend_sensor"] + ["/clock"])
    self.rosbag = subprocess.Popen(
      "rosbag record -a -O experiment_logs.bag",
      stdin=subprocess.PIPE,
      shell=True,
      cwd=self.logs_folder
    )
    return

  def save(self):
    """ Save all data """
    self.flex_sensor.save()
    self.motor_desired_sensor.save()
    self.motor_measured_sensor.save()
    # self.flex_sensor.flex.clock.save()

    # Close rosbags
    self.rosbag.send_signal(subprocess.signal.SIGINT)
    # Compress
    print("Waiting for compression")
    self.rosbag.wait()
    while "experiment_logs.bag" not in os.listdir(self.logs_folder):
      time.sleep(0.5)
    print("Compressing experiment_logs.bag")
    rosbag_compression = subprocess.Popen(
      "rosbag compress experiment_logs.bag",
      stdin=subprocess.PIPE,
      shell=True,
      cwd=self.logs_folder
    )
    rosbag_compression.wait()
    print("Delete previous rosbag backup file")
    os.remove(self.logs_folder + "/experiment_logs.orig.bag")
    print("Compression complete")
    return


def main():
  """ Main """
  # Initialise node
  rospy.init_node("log_experiment", anonymous=False)
  # Log folder
  folder = verify_and_create_log_folder(directory=root_path)
  # Initialise loggers
  loggers = Loggers(folder)
  rospy.on_shutdown(loggers.save)
  rospy.spin()
  return loggers


if __name__ == "__main__":
  main()
