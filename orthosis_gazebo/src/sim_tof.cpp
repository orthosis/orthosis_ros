/**
 * @file sim_tof.cpp
 * @author Dhruv Kool Rajamani (dkoolrajamani@wpi.edu)
 * @brief 
 * @version 0.1
 * @date 2019-11-02
 * 
 * @copyright Copyright (c) 2019
 * 
 */

#pragma region includes
#include <orthosis_gazebo/sim_tof.hpp>

#include <gazebo/physics/World.hh>
#include <gazebo/physics/HingeJoint.hh>
#include <gazebo/sensors/Sensor.hh>
#include <sdf/sdf.hh>
#include <sdf/Param.hh>
#include <gazebo/common/Exception.hh>
#include <gazebo/sensors/RaySensor.hh>
#include <gazebo/sensors/SensorTypes.hh>
#include <gazebo/transport/Node.hh>

#include <geometry_msgs/Point32.h>
#include <sensor_msgs/ChannelFloat32.h>

#include <tf/tf.h>

#include <algorithm>
#include <assert.h>

#include <gazebo_plugins/gazebo_ros_utils.h>
#pragma endregion

#define EPSILON_DIFF 0.000001

namespace gazebo
{
// Register this plugin with the simulator
GZ_REGISTER_SENSOR_PLUGIN(TOF)

// Constructor
TOF::TOF()
{
}

// Destructor
TOF::~TOF()
{
  // Finalize the controller / Custom Callback Queue
  this->laser_queue_.clear();
  this->laser_queue_.disable();
  this->rosnode_->shutdown();
  this->callback_laser_queue_thread_.join();

  delete this->rosnode_;
}

// Load the controller
void TOF::Load(sensors::SensorPtr _parent, sdf::ElementPtr _sdf)
{
  // Load plugin
  RayPlugin::Load(_parent, _sdf);

  // Get the name of the parent sensor
  this->parent_sensor_ = _parent;

  // Get the world name.
  std::string worldName = _parent->WorldName();
  this->world_ = physics::get_world(worldName);

#if GAZEBO_MAJOR_VERSION >= 8
  last_update_time_ = this->world_->SimTime();
#else
  last_update_time_ = this->world_->GetSimTime();
#endif

  this->node_ = transport::NodePtr(new transport::Node());
  this->node_->Init(worldName);

  GAZEBO_SENSORS_USING_DYNAMIC_POINTER_CAST;
  this->parent_ray_sensor_ =
      dynamic_pointer_cast<sensors::RaySensor>(this->parent_sensor_);

  this->robot_namespace_ = "";
  if (_sdf->HasElement("robotNamespace"))
    this
        ->robot_namespace_ =
        _sdf->GetElement("robotNamespace")
            ->Get<std::string>() +
        "/";

  if (!_sdf->HasElement("frameName"))
  {
    ROS_INFO_NAMED(
        "TOF", "TOF plugin missing <frameName>, defaults to /world");

    this->frame_name_ = "/world";
  }
  else
    this->frame_name_ =
        _sdf->GetElement("frameName")
            ->Get<std::string>();

  if (!_sdf->HasElement("topicName"))
  {
    ROS_INFO_NAMED(
        "TOF", "TOF plugin missing <topicName>, defaults to /world");

    this->topic_name_ = "/world";
  }
  else
    this->topic_name_ =
        _sdf->GetElement("topicName")
            ->Get<std::string>();

  if (!_sdf->HasElement("gaussianNoise"))
  {
    ROS_INFO_NAMED(
        "TOF", "TOF plugin missing <gaussianNoise>, defaults to 0.0");

    this->gaussian_noise_ = 0;
  }
  else
    this->gaussian_noise_ =
        _sdf->GetElement("gaussianNoise")
            ->Get<double>();

  if (!_sdf->HasElement("minIntensity"))
  {
    ROS_INFO_NAMED(
        "TOF", "TOF plugin missing <minIntensity>, defaults to 101");

    this->min_intensity_ = 101;
  }
  else
    this->min_intensity_ =
        _sdf->GetElement("minIntensity")
            ->Get<double>();

  ROS_DEBUG_NAMED("TOF",
                  "gazebo_ros_laser plugin should set minimum intensity to %f"
                  "due to cutoff in filters.",
                  this->min_intensity_);

  if (!_sdf->HasElement("updateRate"))
  {
    ROS_INFO_NAMED(
        "TOF", "TOF plugin missing <updateRate>, defaults to 0");

    this->update_rate_ = 0;
  }
  else
    this->update_rate_ =
        _sdf->GetElement("updateRate")->Get<double>();

  this->laser_connect_count_ = 0;

  // Make sure the ROS node for Gazebo has already been initialized
  if (!ros::isInitialized())
  {
    ROS_FATAL_STREAM_NAMED(
        "TOF",
        "A ROS node for Gazebo has not been initialized, "
        "unable to load plugin."
            << "Load the Gazebo system plugin 'vl53l1.so'"
               "in the gazebo_ros package)");

    return;
  }

  this->rosnode_ = new ros::NodeHandle(this->robot_namespace_);

  // resolve tf prefix
  std::string prefix;
  this->rosnode_->getParam(std::string("tf_prefix"), prefix);
  this->frame_name_ = tf::resolve(prefix, this->frame_name_);

  // set size of cloud message, starts at 0!! FIXME: not necessary
  this->cloud_msg_.points.clear();
  this->cloud_msg_.channels.clear();
  this->cloud_msg_.channels.push_back(sensor_msgs::ChannelFloat32());

  this->zonePcl.points.clear();
  this->zonePcl.channels.clear();
  this->zonePcl.channels.push_back(
      sensor_msgs::ChannelFloat32());

  if (this->topic_name_ != "")
  {
    // Custom Callback Queue
    ros::AdvertiseOptions ao =
        ros::AdvertiseOptions::create<sensor_msgs::PointCloud>(
            this->topic_name_,
            1,
            boost::bind(&TOF::LaserConnect, this),
            boost::bind(&TOF::LaserDisconnect, this),
            ros::VoidPtr(), &this->laser_queue_);

    this->pub_ = this->rosnode_->advertise(ao);
  }

  if (_sdf->HasElement("zone"))
  {
    sdf::ElementPtr zoneElement = _sdf->GetElement("zone");
    while (zoneElement)
    {
      this->vec_zones_.push_back(AddZone(zoneElement));

      std::string zoneParam = "zones/" +
                              std::to_string(vec_zones_.back().id);

      rosnode_->setParam(zoneParam + "/Start/X", vec_zones_.back().start_x);
      rosnode_->setParam(zoneParam + "/Start/Y", vec_zones_.back().start_y);
      rosnode_->setParam(zoneParam + "/End/X", vec_zones_.back().end_x);
      rosnode_->setParam(zoneParam + "/End/Y", vec_zones_.back().end_y);

      zoneElement = zoneElement->GetNextElement("zone");
    }
  }
  else
  {
    ROS_INFO_NAMED(
        "TOF", "Adding default zone");

    this->vec_zones_.push_back(AddZone());

    std::string zoneParam = "zones/" +
                            std::to_string(vec_zones_.back().id);

    rosnode_->setParam(zoneParam + "/Start/X", vec_zones_.back().start_x);
    rosnode_->setParam(zoneParam + "/Start/Y", vec_zones_.back().start_y);
    rosnode_->setParam(zoneParam + "/End/X", vec_zones_.back().end_x);
    rosnode_->setParam(zoneParam + "/End/Y", vec_zones_.back().end_y);
  }

  // Initialize the controller

  // sensor generation off by default
  this->parent_ray_sensor_->SetActive(false);
  // start custom queue for laser
  this->callback_laser_queue_thread_ = boost::thread(
      boost::bind(&TOF::LaserQueueThread, this));
}

//////////////////////////////////////////////////////////////////////////
// Increment count
void TOF::LaserConnect()
{
  this->laser_connect_count_++;
  this->parent_ray_sensor_->SetActive(true);
}
//////////////////////////////////////////////////////////////////////////
// Decrement count
void TOF::LaserDisconnect()
{
  this->laser_connect_count_--;

  if (this->laser_connect_count_ == 0)
    this->parent_ray_sensor_->SetActive(false);
}

///////////////////////////////////////////////////////////////////////////
// Update the controller
void TOF::OnNewLaserScans()
{
  if (this->topic_name_ != "")
  {
    common::Time sensor_update_time =
        this->parent_sensor_->LastUpdateTime();
    if (sensor_update_time < last_update_time_)
    {
      ROS_WARN_NAMED(
          "TOF", "Negative sensor update time difference detected.");
      last_update_time_ = sensor_update_time;
    }

    if (last_update_time_ < sensor_update_time)
    {
      this->PutLaserData(sensor_update_time);
      last_update_time_ = sensor_update_time;
    }
  }
  else
  {
    ROS_INFO_NAMED("TOF", "TOF topic name not set");
  }
}

///////////////////////////////////////////////////////////////////////////
// Put laser data to the interface
void TOF::PutLaserData(common::Time &_updateTime)
{
  int i, hja, hjb;
  int j, vja, vjb;
  double vb, hb;
  int j1, j2, j3, j4; // four corners indices
  // four corner values + interpolated range
  double r1, r2, r3, r4, r;
  double intensity;

  this->parent_ray_sensor_->SetActive(false);

  auto maxAngle = this->parent_ray_sensor_->AngleMax();
  auto minAngle = this->parent_ray_sensor_->AngleMin();

  double maxRange = this->parent_ray_sensor_->RangeMax();
  double minRange = this->parent_ray_sensor_->RangeMin();
  int rayCount = this->parent_ray_sensor_->RayCount();
  int rangeCount = this->parent_ray_sensor_->RangeCount();

  int verticalRayCount =
      this->parent_ray_sensor_->VerticalRayCount();
  int verticalRangeCount =
      this->parent_ray_sensor_->VerticalRangeCount();
  auto verticalMaxAngle =
      this->parent_ray_sensor_->VerticalAngleMax();
  auto verticalMinAngle =
      this->parent_ray_sensor_->VerticalAngleMin();

  double yDiff = maxAngle.Radian() - minAngle.Radian();
  double pDiff =
      verticalMaxAngle.Radian() - verticalMinAngle.Radian();

  verticalCount = verticalRangeCount;
  horizontalCount = rangeCount;

  // set size of cloud message everytime!
  //int r_size = rangeCount * verticalRangeCount;
  this->cloud_msg_.points.clear();
  this->cloud_msg_.channels.clear();
  this->cloud_msg_.channels.push_back(
      sensor_msgs::ChannelFloat32());

  this->zonePcl.points.clear();
  this->zonePcl.channels.clear();
  this->zonePcl.channels.push_back(
      sensor_msgs::ChannelFloat32());

  /***************************************************************/
  /*                                                             */
  /*  point scan from laser                                      */
  /*                                                             */
  /***************************************************************/
  boost::mutex::scoped_lock sclock(this->lock);
  // Add Frame Name
  this->cloud_msg_.header.frame_id = this->frame_name_;
  this->cloud_msg_.header.stamp.sec = _updateTime.sec;
  this->cloud_msg_.header.stamp.nsec = _updateTime.nsec;

  this->zonePcl.header.frame_id = this->frame_name_;
  this->zonePcl.header.stamp.sec = _updateTime.sec;
  this->zonePcl.header.stamp.nsec = _updateTime.nsec;

  int count = 0;
  std::vector<double> vec_ranges_;

  for (j = 0; j < verticalRangeCount; j++)
  {
    // interpolating in vertical direction
    vb = (verticalRangeCount == 1) ? 0 : (double)j * ((verticalRayCount - 1) / (verticalRangeCount - 1));
    vja = (int)floor(vb);
    vjb = std::min(vja + 1, verticalRayCount - 1);
    vb = vb - floor(vb); // fraction from min

    assert(vja >= 0 && vja < verticalRayCount);
    assert(vjb >= 0 && vjb < verticalRayCount);

    for (i = 0; i < rangeCount; i++)
    {
      /* Interpolate the range readings from the rays in 
                horizontal direction */
      hb = (rangeCount == 1) ? 0 : (double)i * (rayCount - 1) / (rangeCount - 1);
      hja = (int)floor(hb);
      hjb = std::min(hja + 1, rayCount - 1);
      hb = hb - floor(hb); // fraction from min

      assert(hja >= 0 && hja < rayCount);
      assert(hjb >= 0 && hjb < rayCount);

      // indices of 4 corners
      j1 = hja + vja * rayCount;
      j2 = hjb + vja * rayCount;
      j3 = hja + vjb * rayCount;
      j4 = hjb + vjb * rayCount;
      // range readings of 4 corners
      r1 = std::min(
          this
              ->parent_ray_sensor_
              ->LaserShape()
              ->GetRange(j1),
          maxRange - minRange);
      r2 = std::min(
          this
              ->parent_ray_sensor_
              ->LaserShape()
              ->GetRange(j2),
          maxRange - minRange);
      r3 = std::min(
          this
              ->parent_ray_sensor_
              ->LaserShape()
              ->GetRange(j3),
          maxRange - minRange);
      r4 = std::min(
          this
              ->parent_ray_sensor_
              ->LaserShape()
              ->GetRange(j4),
          maxRange - minRange);

      // Range is linear interpolation if values are close,
      // and min if they are very different
      r = (1 - vb) * ((1 - hb) * r1 + hb * r2) + vb * ((1 - hb) * r3 + hb * r4);

      if ((r <= maxRange) && (r >= maxRange - 0.0001))
        r = -1.0;

      if (r >= 0)
        vec_ranges_.push_back(r);

      // Intensity is averaged
      intensity = 0.25 * (this->parent_ray_sensor_->LaserShape()->GetRetro(j1) +
                          this->parent_ray_sensor_->LaserShape()->GetRetro(j2) +
                          this->parent_ray_sensor_->LaserShape()->GetRetro(j3) +
                          this->parent_ray_sensor_->LaserShape()->GetRetro(j4));

      // get angles of ray to get xyz for point
      double yAngle =
          0.5 * (hja + hjb) * (yDiff / (rayCount - 1)) +
          minAngle.Radian();
      double pAngle =
          0.5 * (vja + vjb) * (pDiff / (verticalRayCount - 1)) +
          verticalMinAngle.Radian();

      /***************************************************************/
      /*                                                             */
      /*  point scan from laser                                      */
      /*                                                             */
      /***************************************************************/
      //compare 2 doubles
      double diffRange = maxRange - minRange;
      double diff = diffRange - r;
      if (fabs(diff) < EPSILON_DIFF)
      {
        // no noise if at max range
        geometry_msgs::Point32 point;
        //pAngle is rotated by yAngle:
        point.x = r * cos(pAngle) * cos(yAngle);
        point.y = r * cos(pAngle) * sin(yAngle);
        point.z = r * sin(pAngle);

        this->vec_points_.push_back(point);
        this->zonePcl.points.push_back(point);
      }
      else
      {
        geometry_msgs::Point32 point;
        //pAngle is rotated by yAngle:
        point.x =
            (r * cos(pAngle) * cos(yAngle)) +
            this->GaussianKernel(0, this->gaussian_noise_);
        point.y =
            (r * cos(pAngle) * sin(yAngle)) +
            this->GaussianKernel(0, this->gaussian_noise_);
        point.z =
            (r * sin(pAngle)) +
            this->GaussianKernel(0, this->gaussian_noise_);

        this->zonePcl.points.push_back(point);

      } // only 1 channel

      this->zonePcl.channels[0].values.push_back(
          intensity +
          this->GaussianKernel(0, this->gaussian_noise_));
      count++;
    }
  }

  ZoneToPcl(vec_ranges_, maxRange);

  this->parent_ray_sensor_->SetActive(true);

  //////////////////////////////////////////////////////////////////////
  // send data out via ros message
  this->pub_.publish(this->cloud_msg_);
}

//////////////////////////////////////////////////////////////////////////
// Utility for adding noise
double TOF::GaussianKernel(double mu, double sigma)
{
  // using Box-Muller transform to generate two independent
  // standard normally disbributed normal variables

  // normalized uniform random variable
  double U = (double)rand() / (double)RAND_MAX;
  // normalized uniform random variable
  double V = (double)rand() / (double)RAND_MAX;
  double X = sqrt(-2.0 * ::log(U)) * cos(2.0 * M_PI * V);
  // scale to our mu and sigma
  X = sigma * X + mu;

  return X;
}

// Custom Callback Queue
///////////////////////////////////////////////////////////////////////////
// custom callback queue thread
void TOF::LaserQueueThread()
{
  static const double timeout = 0.01;

  while (this->rosnode_->ok())
  {
    this->laser_queue_.callAvailable(ros::WallDuration(timeout));
  }
}

void TOF::OnStats(
    const boost::shared_ptr<msgs::WorldStatistics const> &_msg)
{
  this->sim_time_ = msgs::Convert(_msg->sim_time());

  ignition::math::Pose3d pose;
  pose.Pos().X() = 0.5 * sin(0.01 * this->sim_time_.Double());
  gzdbg << "plugin simTime [" << this->sim_time_.Double() << "] update pose [" << pose.Pos().X() << "]\n";
}

zones TOF::AddZone(sdf::ElementPtr zoneElement)
{
  zones zone;

  // Get zone ID
  if (!zoneElement->HasElement("id"))
  {
    ROS_INFO_NAMED(
        "ZONE", "Adding default zone id: 0");
    zone.id = 0;
  }
  else
  {
    zone.id = zoneElement->GetElement("id")->Get<int>();
  }

/////////////////////////////////////////////////////////////////////
// Get Zone start positions
#pragma region zone_pos
  if (!zoneElement->HasElement("startX"))
  {
    ROS_INFO_NAMED(
        "ZONE", "Adding default zone startX: 0");
    zone.start_x = 0;
  }
  else
  {
    zone.start_x = zoneElement->GetElement("startX")->Get<int>();
  }

  if (!zoneElement->HasElement("startY"))
  {
    ROS_INFO_NAMED(
        "ZONE", "Adding default zone startY: 0");
    zone.start_y = 0;
  }
  else
  {
    zone.start_y = zoneElement->GetElement("startY")->Get<int>();
  }

  if (!zoneElement->HasElement("endX"))
  {
    ROS_INFO_NAMED(
        "ZONE", "Adding default zone endX: 0");
    zone.end_x = 15;
  }
  else
  {
    zone.end_x = zoneElement->GetElement("endX")->Get<int>();
  }

  if (!zoneElement->HasElement("endY"))
  {
    ROS_INFO_NAMED(
        "ZONE", "Adding default zone endY: 0");
    zone.end_y = 15;
  }
  else
  {
    zone.end_y = zoneElement->GetElement("endY")->Get<int>();
  }
#pragma endregion

  return zone;
}

zones TOF::AddZone()
{
  zones zone;

  ROS_INFO_NAMED(
      "ZONE", "Adding default zone id: 0");
  zone.id = 0;

/////////////////////////////////////////////////////////////////////
// Get Zone start positions
#pragma region zone_pos
  ROS_INFO_NAMED(
      "ZONE", "Adding default zone startX: 0");
  zone.start_x = 0;

  ROS_INFO_NAMED(
      "ZONE", "Adding default zone startY: 0");
  zone.start_y = 0;

  ROS_INFO_NAMED(
      "ZONE", "Adding default zone endX: 0");
  zone.end_x = 15;

  ROS_INFO_NAMED(
      "ZONE", "Adding default zone endY: 0");
  zone.end_y = 15;
#pragma endregion

  return zone;
}

void TOF::ZoneToPcl(std::vector<double> vec_ranges_, double maxRange)
{
  for (
      std::vector<zones>::iterator it = vec_zones_.begin();
      it != vec_zones_.end();
      it++)
  {
    zones tempZone = *it;
    double intensity_;
    std::vector<double> ranges_;
    std::vector<double>::iterator rIt;
    geometry_msgs::Point32 point;
    double r;

    for (int i = 0; i < zonePcl.points.size(); i++)
    {
      int row = i / verticalCount;
      int col = i % horizontalCount;

      if (
          row <= tempZone.end_x &&
          col <= tempZone.end_y &&
          row >= tempZone.start_x &&
          col >= tempZone.start_y)
      {
        for (int ii = tempZone.start_x; ii < tempZone.end_x; ii++)
          for (int jj = tempZone.start_y; jj < tempZone.end_y; jj++)
          {
            ranges_.push_back(vec_ranges_[(ii * verticalCount) + jj]);
          }

        r = *std::min_element(ranges_.begin(), ranges_.end());

        int r_id = 0;
        for (rIt = vec_ranges_.begin(); rIt != vec_ranges_.end(); rIt++)
        {
          if (*rIt == r)
          {
            break;
          }
          else
            r_id++;
        }

        point = zonePcl.points[r_id];
        intensity_ = zonePcl.channels[0].values[r_id];
      }
    }
    this->cloud_msg_.points.push_back(point);
    this->cloud_msg_.channels[0].values.push_back(intensity_);
  }
}
} // namespace gazebo