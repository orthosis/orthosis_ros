#!/usr/bin/python3
from __future__ import print_function
import numpy as np
import cv2
import imutils

def auto_canny(image, sigma=0.35):
    # compute the median of the single channel pixel intensities
    v = np.median(image)
    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
    # return the edged image
    return edged

# Load the original image
print('Loading the original image...')
original = cv2.imread('predictions2.jpg')
######################################

# # Adjust brightness and contrast
# print('Adjusting brightness and contrast...')
# # Contrast gain
# alpha = 2.8
# # Brightness bias
# beta = 15

# img_corrected = np.zeros(original.shape, original.dtype)

# # Pixel-wise operation
# for y in range(original.shape[0]):
#     for x in range(original.shape[1]):
#         for c in range(original.shape[2]):
#             original[y,x,c] = np.clip(alpha*original[y,x,c] + beta, 0, 255)
# ##########################################

# # Gamma correction
# print('Performing gamma correction...')
# gamma_thresh = 1.2
# # Build a lookup table mapping the pixel values [0, 255] to their adjusted gamma values
# invGamma = 1.0 / gamma_thresh
# table = np.array([((i / 255.0) ** invGamma) * 255
#     for i in np.arange(0, 256)]).astype("uint8")
 
# # Apply gamma correction using the lookup table
# original = cv2.LUT(original, table)
# ##########################################

# Histogram equalization
print('Equalizing the Histogram...')
# Convert to YUV color space to preserve color info
img_yuv = cv2.cvtColor(original, cv2.COLOR_BGR2YUV)

# Implement Contrast Limited Adaptive Histogram Equalization
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
img_yuv[:,:,0] = clahe.apply(img_yuv[:,:,0])

original = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)
# ###########################################


# Edge detection
print('Detecting Edge...')

gray = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
# gray = cv2.GaussianBlur(gray, (3, 3), 0)
# # threshold the image, then perform a series of erosions +
# dilations to remove any small regions of noise

gray = cv2.bilateralFilter(gray, 11, 17, 17)
# thresh = auto_canny(gray,.9)
thresh = cv2.threshold(gray,50,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)[1]
# thresh = cv2.erode(thresh, None, iterations=6)
# thresh = cv2.dilate(thresh, None, iterations=1)
# ret, thresh = cv2.threshold(gray, 50, 255, cv2.THRESH_BINARY)
# ret, thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)

# # noise removal
# kernel = np.ones((3,3),np.uint8)
# opening = cv2.morphologyEx(thresh,cv2.MORPH_OPEN,kernel, iterations = 2)
# # sure background area
# sure_bg = cv2.dilate(opening,kernel,iterations=3)
# # Finding sure foreground area
# dist_transform = cv2.distanceTransform(opening,cv2.DIST_L2,5)
# ret, sure_fg = cv2.threshold(dist_transform,0.7*dist_transform.max(),255,0)
# # Finding unknown region
# sure_fg = np.uint8(sure_fg)
# unknown = cv2.subtract(sure_bg,sure_fg)
# # Marker labelling
# ret, markers = cv2.connectedComponents(sure_fg)
# # Add one to all labels so that sure background is not 0, but 1
# markers = markers+1
# # Now, mark the region of unknown with zero
# markers[unknown==255] = 0
# markers = cv2.watershed(original,markers)
# original[markers == -1] = [255,0,0]

contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
# create hull array for convex hull points
hull = []
 
# calculate points for each contour
for i in range(len(contours)):
    # creating convex hull object for each contour
    hull.append(cv2.convexHull(contours[i], False))

# create an empty black image
drawing = np.zeros((thresh.shape[0], thresh.shape[1], 3), np.uint8)
 
# draw contours and hull points
for i in range(len(contours)):
    color_contours = (0, 255, 0) # green - color for contours
    color = (255, 0, 0) # blue - color for convex hull
    # draw ith contour
    # cv2.drawContours(original, contours, i, color_contours, 1, 8, hierarchy)
    # draw ith convex hull object
    cv2.drawContours(original, hull, i, color, 1, 8)

# cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
#     cv2.CHAIN_APPROX_SIMPLE)
# cnts = imutils.grab_contours(cnts)
# c = max(cnts, key=cv2.contourArea)
# cv2.drawContours(original, [c], -1, (0, 255, 255), 2)


# # apply Canny edge detection using an automatically determined threshold
auto = auto_canny(gray,0.37)



# show the images
cv2.imwrite('image_edged.jpg',original)
cv2.imshow("Edges", original) #np.hstack([gray, auto]))
cv2.waitKey(0)