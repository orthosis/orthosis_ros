#!usr/bin/python3
from ctypes import *
import math
import random
import cv2
import os
import imutils
import glob
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import numpy as np
from random import randint
import rospy
import random
# from utils import detector_utils as detector_utils
# from camera.rgb import image_raw
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
# from sensor_msgs.msg import PointCloud2, PointField
import time
# import tensorflow as tf
from cv2 import aruco

global image_raw
global count
count = 0
# detection_graph, sess = detector_utils.load_inference_graph()


        
def calibrate():
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    # checkerboard of size (7 x 6) is used
    objp = np.zeros((6*7,3), np.float32)
    objp[:,:2] = np.mgrid[0:7,0:6].T.reshape(-1,2)

    # arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.

    # iterating through all calibration images
    # in the folder
    images = glob.glob('calib_images/*.jpg')

    for fname in images:
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

        # find the chess board (calibration pattern) corners
        ret, corners = cv2.findChessboardCorners(gray, (7,6),None)

        # if calibration pattern is found, add object points,
        # image points (after refining them)
        if ret == True:
            objpoints.append(objp)

            # Refine the corners of the detected corners
            corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
            imgpoints.append(corners2)

            # Draw and display the corners
            img = cv2.drawChessboardCorners(img, (7,6), corners2,ret)


    return objpoints, imgpoints, gray

def sample(probs):
    s = sum(probs)
    probs = [a/s for a in probs]
    r = random.uniform(0, 1)
    for i in range(len(probs)):
        r = r - probs[i]
        if r <= 0:
            return i
    return len(probs)-1

def c_array(ctype, values):
    arr = (ctype*len(values))()
    arr[:] = values
    return arr

class BOX(Structure):
    _fields_ = [("x", c_float),
                ("y", c_float),
                ("w", c_float),
                ("h", c_float)]

class DETECTION(Structure):
    _fields_ = [("bbox", BOX),
                ("classes", c_int),
                ("prob", POINTER(c_float)),
                ("mask", POINTER(c_float)),
                ("objectness", c_float),
                ("sort_class", c_int)]


class IMAGE(Structure):
    _fields_ = [("w", c_int),
                ("h", c_int),
                ("c", c_int),
                ("data", POINTER(c_float))]

class METADATA(Structure):
    _fields_ = [("classes", c_int),
                ("names", POINTER(c_char_p))]

    


class IplROI(Structure):
    pass

class IplTileInfo(Structure):
    pass

class IplImage(Structure):
    pass

IplImage._fields_ = [
    ('nSize', c_int),
    ('ID', c_int),
    ('nChannels', c_int),               
    ('alphaChannel', c_int),
    ('depth', c_int),
    ('colorModel', c_char * 4),
    ('channelSeq', c_char * 4),
    ('dataOrder', c_int),
    ('origin', c_int),
    ('align', c_int),
    ('width', c_int),
    ('height', c_int),
    ('roi', POINTER(IplROI)),
    ('maskROI', POINTER(IplImage)),
    ('imageId', c_void_p),
    ('tileInfo', POINTER(IplTileInfo)),
    ('imageSize', c_int),          
    ('imageData', c_char_p),
    ('widthStep', c_int),
    ('BorderMode', c_int * 4),
    ('BorderConst', c_int * 4),
    ('imageDataOrigin', c_char_p)]


class iplimage_t(Structure):
    _fields_ = [('ob_refcnt', c_ssize_t),
                ('ob_type',  py_object),
                ('a', POINTER(IplImage)),
                ('data', py_object),
                ('offset', c_size_t)]


#lib = CDLL("/home/pjreddie/documents/darknet/libdarknet.so", RTLD_GLOBAL)
lib = CDLL("/media/raunak/OS/ADI_challenge/Yolov3/darknet/libdarknet.so", RTLD_GLOBAL)
lib.network_width.argtypes = [c_void_p]
lib.network_width.restype = c_int
lib.network_height.argtypes = [c_void_p]
lib.network_height.restype = c_int

predict = lib.network_predict
predict.argtypes = [c_void_p, POINTER(c_float)]
predict.restype = POINTER(c_float)

set_gpu = lib.cuda_set_device
set_gpu.argtypes = [c_int]

make_image = lib.make_image
make_image.argtypes = [c_int, c_int, c_int]
make_image.restype = IMAGE

get_network_boxes = lib.get_network_boxes
get_network_boxes.argtypes = [c_void_p, c_int, c_int, c_float, c_float, POINTER(c_int), c_int, POINTER(c_int)]
get_network_boxes.restype = POINTER(DETECTION)

make_network_boxes = lib.make_network_boxes
make_network_boxes.argtypes = [c_void_p]
make_network_boxes.restype = POINTER(DETECTION)

free_detections = lib.free_detections
free_detections.argtypes = [POINTER(DETECTION), c_int]

free_ptrs = lib.free_ptrs
free_ptrs.argtypes = [POINTER(c_void_p), c_int]

network_predict = lib.network_predict
network_predict.argtypes = [c_void_p, POINTER(c_float)]

reset_rnn = lib.reset_rnn
reset_rnn.argtypes = [c_void_p]

load_net = lib.load_network
load_net.argtypes = [c_char_p, c_char_p, c_int]
load_net.restype = c_void_p

do_nms_obj = lib.do_nms_obj
do_nms_obj.argtypes = [POINTER(DETECTION), c_int, c_int, c_float]

do_nms_sort = lib.do_nms_sort
do_nms_sort.argtypes = [POINTER(DETECTION), c_int, c_int, c_float]

free_image = lib.free_image
free_image.argtypes = [IMAGE]

letterbox_image = lib.letterbox_image
letterbox_image.argtypes = [IMAGE, c_int, c_int]
letterbox_image.restype = IMAGE

load_meta = lib.get_metadata
lib.get_metadata.argtypes = [c_char_p]
lib.get_metadata.restype = METADATA

load_image = lib.load_image_color
load_image.argtypes = [c_char_p, c_int, c_int]
load_image.restype = IMAGE

rgbgr_image = lib.rgbgr_image
rgbgr_image.argtypes = [IMAGE]

predict_image = lib.network_predict_image
predict_image.argtypes = [c_void_p, IMAGE]
predict_image.restype = POINTER(c_float)


def classify(net, meta, im):
    out = predict_image(net, im)
    res = []
    for i in range(meta.classes):
        res.append((meta.names[i], out[i]))
    res = sorted(res, key=lambda x: -x[1])
    return res


def array_to_image(arr):
    # need to return old values to avoid python freeing memory
    arr = arr.transpose(2,0,1)
    c, h, w = arr.shape[0:3]
    arr = np.ascontiguousarray(arr.flat, dtype=np.float32) / 255.0
    data = arr.ctypes.data_as(POINTER(c_float))
    im = IMAGE(w,h,c,data)
    return im, arr

def detect(net, meta, image, thresh=.5, hier_thresh=.5, nms=.45):
    """if isinstance(image, bytes):  
        # image is a filename 
        # i.e. image = b'/darknet/data/dog.jpg'
        im = load_image(image, 0, 0)
    else:  
        # image is an nparray
        # i.e. image = cv2.imread('/darknet/data/dog.jpg')
        im, image = array_to_image(image)
        rgbgr_image(im)
    """
    im, image = array_to_image(image)
    rgbgr_image(im)
    num = c_int(0)
    pnum = pointer(num)
    predict_image(net, im)
    dets = get_network_boxes(net, im.w, im.h, thresh, 
                             hier_thresh, None, 0, pnum)
    num = pnum[0]
    if nms: do_nms_obj(dets, num, meta.classes, nms)

    res = []
    for j in range(num):
        a = dets[j].prob[0:meta.classes]
        if any(a):
            ai = np.array(a).nonzero()[0]
            for i in ai:
                b = dets[j].bbox
                res.append((meta.names[i], dets[j].prob[i], 
                           (b.x, b.y, b.w, b.h)))

    res = sorted(res, key=lambda x: -x[1])
    if isinstance(image, bytes): free_image(im)
    free_detections(dets, num)
    return res

def template_match(scene,templates):
    global prev_frame
    obj_center = []
    
    for i in range(len(templates)):
        template = prev_frame[templates[i][1]:templates[i][3],templates[i][0]:templates[i][2]].copy()
        try:
            img1 = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
        except cv2.error as e:
            break
        img2 = cv2.cvtColor(scene, cv2.COLOR_BGR2GRAY)

        (tH, tW) = template.shape[:2]
        (sH, sW) = scene.shape[:2]

        # # load the image, convert it to grayscale, and initialize the
        gray = cv2.cvtColor(scene, cv2.COLOR_BGR2GRAY)
        found = None

        # loop over the scales of the image
        for scale in np.linspace(0.5, 1.0, 5)[::-1]:
                # resize the image according to the scale, and keep track
                # of the ratio of the resizing
                resized = imutils.resize(gray, width = int(gray.shape[1] * scale))
                r = gray.shape[1] / float(resized.shape[1])
         
                # if the resized image is smaller than the template, then break
                # from the loop
                if resized.shape[0] < tH or resized.shape[1] < tW:
                    break

                result = cv2.matchTemplate(resized, img1, cv2.TM_CCOEFF_NORMED)
                (_, maxVal, _, maxLoc) = cv2.minMaxLoc(result)
         
                # check to see if the iteration should be visualized
                
                # if we have found a new maximum correlation value, then update
                # the bookkeeping variable
                if found is None or maxVal > found[0]:
                    found = (maxVal, maxLoc, r)
         
        # unpack the bookkeeping variable and compute the (x, y) coordinates
        # of the bounding box based on the resized ratio
        (_, maxLoc, r) = found
        (startX, startY) = (int(maxLoc[0] * r), int(maxLoc[1] * r))
        (endX, endY) = (int((maxLoc[0] + tW) * r), int((maxLoc[1] + tH) * r))
        # print(startX, startY, endX, endY)

        cv2.rectangle(scene, (startX, startY), (endX, endY), templates[i][4], 2)
        obj_center.append((int(startX/2+endX/2), int(startY/2+endY/2)))

    return scene, obj_center

def get_vid_callback(data):
    
    global image_raw
    stime= time.time()

    try:
        image_raw = bridge.imgmsg_to_cv2(data, "passthrough")
    except CvBridgeError as e:
        rospy.logerr("CvBridge Error: {0}".format(e))

    rgb_frames = cv2.cvtColor(image_raw, cv2.COLOR_BGR2RGB)

    scale_percent = 75 # percent of original size
    width = int(rgb_frames.shape[1] * scale_percent / 100)
    height = int(rgb_frames.shape[0] * scale_percent / 100)
    dim = (width, height)
    # resize image
    rgb_frame = cv2.resize(rgb_frames, dim, interpolation = cv2.INTER_AREA)
    im, arr = array_to_image(rgb_frame)

    # video = cv2.VideoCapture(vid_source)
    global objs
    global count
    if count%8==0:
        count = 0
        objs = []
        obj_center = []
        classes_box_colors = [tuple(255 * np.random.rand(3)) for i in range(100)]
        thresh=.2
        hier_thresh=.2
        nms=.45
        # res, frame = video.read()
        # if not res:
        #     break        
        global prev_frame
        prev_frame = rgb_frame    
        num = c_int(0)
        pnum = pointer(num)
        predict_image(net, im)
        dets = get_network_boxes(net, im.w, im.h, thresh, hier_thresh, None, 0, pnum)
        num = pnum[0]
        if (nms): do_nms_obj(dets, num, meta.classes, nms);
        res = []
        # print('FPS {:1f} :'.format(1/(time.time() - stime)))

        for j in range(num):

                for i in range(meta.classes):

                    if dets[j].prob[i] > 0:
                        classes_box_color = COLORS[i]

                        b = dets[j].bbox
                        x1 = int(b.x - b.w / 2.)
                        y1 = int(b.y - b.h / 2.)
                        x2 = int(b.x + b.w / 2.)
                        y2 = int(b.y + b.h / 2.)
                        # print(meta.names[i].decode("utf-8"),' - ',dets[j].prob[i])
                        # res.append((meta.names[i].decode("utf-8"), dets[j].prob[i], (b.x, b.y, b.w, b.h), i))
                        cv2.rectangle(rgb_frame, (x1, y1), (x2, y2), classes_box_color, 2)
                        objs.append([x1,y1,x2,y2,classes_box_color])
                        obj_center.append((int(x1/2+x2/2),int(y1/2+y2/2)))
                        # cv2.putText(rgb_frame, str(meta.names[i].decode("utf-8"))+': '+str(int(np.round(dets[j].prob[i],2)*100))+'%', (x1, y1 - 20), 1, 1, classes_box_color, 2, cv2.LINE_AA)
                # res = sorted(res, key=lambda x: -x[1])
        count = count + 1

    else:
        rgb_frame, obj_center = template_match(rgb_frame,objs)
        count = count + 1
        # print('FPS {:1f} :'.format(1/(time.time() - stime)))

    # boxes, scores = detector_utils.detect_objects(rgb_frame,detection_graph, sess)

    # # draw bounding boxes on frame
    # detector_utils.draw_box_on_image(1, 0.2,scores, boxes, rgb_frame.shape[1],rgb_frame.shape[0],rgb_frame)
    objpoints, imgpoints, gray = calibrate()
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)

    # gray = cv2.cvtColor(rgb_frame, cv2.COLOR_BGR2GRAY)
    # aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    # parameters =  aruco.DetectorParameters_create()
    # corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    # # aruco.drawDetectedMarkers(rgb_frame, corners, ids)
    # rvec, tvec ,_ = aruco.estimatePoseSingleMarkers(corners, 0.05, mtx, dist)
    # #(rvec-tvec).any() # get rid of that nasty numpy value array error
    # x = []
    # y = []
    # z = []
    # try:    
    #     for i in range(len(ids)):
    #         aruco.drawAxis(rgb_frame, mtx, dist, rvec[i], tvec[i], 0.04)
    #         c = corners[i][0]
    #         hand_center = (c[:, 0].mean(),c[:, 1].mean())
    #         cv2.circle(rgb_frame,hand_center, 2, (0,0,255), -1)
    #         # x.append(rvec[i][0][0])
    #         # y.append(rvec[i][0][1])
    #         # z.append(rvec[i][0][2])
    #         for j in range(len(obj_center)):
    #             # print(obj_center[j], hand_center)
    #             cv2.circle(rgb_frame,obj_center[j], 2, (0,0,255), -1)
    #             cv2.line(rgb_frame,hand_center,obj_center[j],(255,0,0),1)
    #     # print('Detected markers - ',len(ids), np.mean(x)*180/np.pi, np.mean(y)*180/np.pi, np.mean(z)*180/np.pi)

    # except TypeError as e:
    #     # print('No markers detected')
    #     pass


    frame2 = cv2.resize(rgb_frame, (1600, 900))
    cv2.namedWindow("output", cv2.WINDOW_NORMAL)        
    cv2.imshow('output', frame2)
    # print(obj_center)
    if cv2.waitKey(1) == ord('q'):
        return        
    # print (res)
    


if __name__ == "__main__":
    set_gpu(0)
    rospy.init_node('get_camera',anonymous=True)

    # os.system('source ~/catkin_ws/devel/setup.bash')
    global net
    global meta
    global COLORS
    # global count
    global prev_frame
    global objs
    COLORS = np.random.uniform(0, 255, size=(9001, 3))
    net = load_net("cfg/yolov3-tiny.cfg".encode('ascii'), "weights/yolov3-tiny.weights".encode('ascii'), 0)
    meta = load_meta("cfg/coco.data".encode('ascii'))
    vid_source = 0
    bridge = CvBridge()
    rospy.Subscriber("/camera/color/image_raw", Image, get_vid_callback)
    rospy.spin()
    