![Darknet Logo](http://pjreddie.com/media/files/darknet-black-small.png)

# Darknet #
Darknet is an open source neural network framework written in C and CUDA. It is fast, easy to install, and supports CPU and GPU computation.

For more information see the [Darknet project website](http://pjreddie.com/darknet).

For questions or issues please use the [Google Group](https://groups.google.com/forum/#!forum/darknet).


To launch realsense camera:
roslaunch realsense2_camera rs_camera.launch
(ref: https://github.com/IntelRealSense/realsense-ros)

To run object detection:
python3 examples/detector_vid.py