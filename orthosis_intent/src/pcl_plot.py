from __future__ import print_function

import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn import preprocessing
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import path
import rospy
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
import sensor_msgs.point_cloud2 as pc2
import pcl.pcl_visualization
import ros_numpy
from sensor_msgs.msg import PointCloud2, PointField
import pcl
import ctypes
import struct
import std_msgs.msg
import tf2_ros
import tf2_py as tf2
# from tf2_sensor_msgs.tf2_sensor_msgs import do_transform_cloud

# tf_buffer = tf2_ros.Buffer()
# tf_listener = tf2_ros.TransformListener(tf_buffer)

def XYZRGB_to_XYZ(XYZRGB_cloud):
    """ Converts a PCL XYZRGB point cloud to an XYZ point cloud (removes color info)
        Args:
            XYZRGB_cloud (PointCloud_PointXYZRGB): A PCL XYZRGB point cloud
        Returns:
            PointCloud_PointXYZ: A PCL XYZ point cloud
    """
    XYZ_cloud = pcl.PointCloud()
    points_list = []

    for data in XYZRGB_cloud:
        points_list.append([data[0], data[1], data[2]])

    XYZ_cloud.from_list(points_list)
    return XYZ_cloud

def ros_to_pcl(ros_cloud):
    """ Converts a ROS PointCloud2 message to a pcl PointXYZRGB
        Args:
            ros_cloud (PointCloud2): ROS PointCloud2 message
        Returns:
            pcl.PointCloud_PointXYZRGB: PCL XYZRGB point cloud
    """
    points_list = []

    for data in pc2.read_points(ros_cloud, skip_nans=True):
        points_list.append([data[0], data[1], data[2], data[3]])

    pcl_data = pcl.PointCloud_PointXYZRGB()
    pcl_data.from_list(points_list)

    data = XYZRGB_to_XYZ(pcl_data)

    return np.asarray(data),pcl_data


def crop_box(cloud,x1,y1,z1,x2,y2,z2):

    cropped = np.array(np.shape(cloud))
    try:
        for i in range(len(cloud)):
            if cloud[i,0]>x1-5 and cloud[i,0]<x2+5:
                if cloud[i,1]>y1-5 and cloud[i,1]<y2+5:
                    cropped[i,:] = cloud[i,:]
    except TypeError as e:
        pass
    return cropped 

def get_image(pcl_data):
    rgb = np.array()
    for data in pcl_data:
        s = struct.pack('>f', data[3])
        i = struct.unpack('>l', s)[0]
        pack = ctypes.c_uint32(i).value

        r = (pack & 0x00FF0000) >> 16
        g = (pack & 0x0000FF00) >> 8
        b = (pack & 0x000000FF)

        x = data[0]
        y = data[1]

def get_ros(pcl_array):
    """ Converts a pcl PointXYZRGB to a ROS PointCloud2 message
        Args:
            pcl_array (PointCloud_PointXYZRGB): A PCL XYZRGB point cloud
        Returns:
            PointCloud2: A ROS point cloud
    """
    # ros_msg = PointCloud2()

    # ros_msg.header.stamp = rospy.Time.now()
    # ros_msg.header.frame_id = "world"

    # ros_msg.height = 1
    # ros_msg.width = pcl_array.size

    # ros_msg.fields.append(PointField(
    #                         name="x",
    #                         offset=0,
    #                         datatype=PointField.FLOAT32, count=1))
    # ros_msg.fields.append(PointField(
    #                         name="y",
    #                         offset=4,
    #                         datatype=PointField.FLOAT32, count=1))
    # ros_msg.fields.append(PointField(
    #                         name="z",
    #                         offset=8,
    #                         datatype=PointField.FLOAT32, count=1))
    # ros_msg.fields.append(PointField(
    #                         name="rgb",
    #                         offset=16,
    #                         datatype=PointField.FLOAT32, count=1))

    # ros_msg.is_bigendian = False
    # ros_msg.point_step = 32
    # ros_msg.row_step = ros_msg.point_step * ros_msg.width * ros_msg.height
    # ros_msg.is_dense = False
    # buffer = []

    # for data in pcl_array:
    #     s = struct.pack('>f', data[3])
    #     i = struct.unpack('>l', s)[0]
    #     pack = ctypes.c_uint32(i).value

    #     r = (pack & 0x00FF0000) >> 16
    #     g = (pack & 0x0000FF00) >> 8
    #     b = (pack & 0x000000FF)

    #     buffer.append(struct.pack('ffffBBBBIII', data[0], data[1], data[2], 1.0, b, g, r, 0, 0, 0, 0))

    # ros_msg.data = "".encode('ascii').join(buffer)

    #header
    rospy.sleep(1.)

    header = std_msgs.msg.Header()
    header.stamp = rospy.Time.now()
    header.frame_id = 'map'
    #create pcl from points
    scaled_polygon_pcl = pc2.create_cloud_xyz32(header, pcl_array)

    try:
        trans = tf_buffer.lookup_transform("", header.frame_id,
                                           header.stamp,
                                           rospy.Duration(timeout))
    except tf2.LookupException as ex:
        rospy.logwarn(ex)
        return
    except tf2.ExtrapolationException as ex:
        rospy.logwarn(ex)
        return
    cloud_out = do_transform_cloud(scaled_polygon_pcl, trans)


    return cloud_out

def get_vid_callback(data):
    pcl_array,pcl_data = ros_to_pcl(data)
    x1,y1,x2,y2 = 5, 93, 133, 250
    print(np.shape(np.asarray(pcl_array)))
    print(len(np.asarray(pcl_array[:,1])))
    # get_image(pcl_data)
    # crop_pcl_data = crop_box(pcl_array,x1,y1,0,x2,y2,15)
    # visualize(crop_pcl_data)
    # point_msg = get_ros(pcl_data)
    # pub = rospy.Publisher('PC', PointCloud2, queue_size=10)
    # pub.publish(point_msg)
    

if __name__ == "__main__":
    # set_gpu(0)
    rospy.init_node('get_camera',anonymous=True)

    vid_source = 0
    bridge = CvBridge()
    rospy.Subscriber("/camera/depth/color/points", PointCloud2, get_vid_callback)
    rospy.spin()