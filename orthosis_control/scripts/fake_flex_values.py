#!/usr/bin/python
""" This program subscribes to bend sensors and publishes to joint states
    # Add more documentation here.
"""

__author__ = "Dhruv Kool Rajamani"
__email__ = "dkoolrajamani@wpi.edu"
__copyright__ = "Copyright 2020"
__date__ = "05 February 2020"

import sys
import rospy
from std_msgs.msg import Float64  # , String
from sensor_msgs.msg import JointState
from bend_sensor_msg.msg import bend_sensor
from std_msgs.msg import Float32
import time
import numpy as np
import rospkg
import roslaunch
import subprocess
import os
import random

root_path = "/home/dhruv/catkin_ws/src/orthosis_ros/orthosis_analysis"
fitting_path = root_path + '/scripts/'
sys.path.append(fitting_path)

from fitting import predict_values, model_load
from fitting import scale_sample, load_dataset
import tensorflow as tf
import keras

global X
global y
global joints
global fingers
global bend_sensor_pubs
global bend_sensor_thruths_pubs
global count

X = None
y = None
count = 0

joints = None
fingers = None
bend_sensor_pubs = None
bend_sensor_thruths_pubs = None


def returnFingerName(joint):

  finger = joint[:joint.find("_joint")]

  return finger


def main():
  """ main """
  global count
  (m, n) = np.shape(X)
  rospy.init_node("fake_flex_values_publisher_py", anonymous=False)
  rospy.loginfo("fake_flex_values_publisher_py has been initialized")
  r = rospy.Rate(50)  # 10hz
  while not rospy.is_shutdown():
    for pub in bend_sensor_pubs:
      i = count
      # i = random.randint(0, m - 1)
      msg = bend_sensor()
      bend_msg = Float32()
      stretch_msg = Float32()
      bend_msg.data = X[i][0]
      msg.bend = bend_msg
      stretch_msg.data = X[i][1]
      msg.stretch = stretch_msg
      msg.header.stamp = rospy.Time.now()
      pub.publish(msg)

      msgTruths = [Float64(), Float64()]
      msgTruths[0].data = y[i][0] * -np.pi / 180
      msgTruths[1].data = y[i][1] * -np.pi / 180
      bend_sensor_thruths_pubs[0].publish(msgTruths[0])
      bend_sensor_thruths_pubs[1].publish(msgTruths[1])

      rospy.loginfo(
        "Finger: {},\tBend: {}, Stretch: {}".format(
          pub.name,
          msg.bend,
          msg.stretch
        )
      )
      rospy.loginfo(
        "Joint: {},\tTheta1: {}, Theta2: {}".format(
          pub.name,
          msgTruths[0].data,
          msgTruths[1].data
        )
      )

    if count < m - 1:
      count += 1
    else:
      count = 0
    r.sleep()

  return


def initialize_model():
  global X
  global y

  X, y = load_dataset([9])

  return


def initialize_publishers_and_subscribers():
  """ Subscribe to joints coming from bend sensors """
  global fingers
  global bend_sensor_pubs
  global bend_sensor_thruths_pubs

  fingers_set = set([returnFingerName(joint) for joint in joints])
  fingers = list(fingers_set)
  bend_sensor_pubs = [
    rospy.Publisher(
      "/devices/{}/bend_sensor".format(finger),
      bend_sensor,
      queue_size=10
    ) for finger in fingers
  ]

  bend_sensor_thruths_pubs = [
    rospy.Publisher(
      "/devices/{}/bend_sensor_truths/".format(joint),
      Float64,
      queue_size=10
    ) for joint in joints
  ]

  return


if __name__ == "__main__":
  try:
    global joints
    joints = rospy.get_param(
      "/orthosis/hw_config_joints",
      ["index_joint1",
       "index_joint2"]
    )
    initialize_model()
    initialize_publishers_and_subscribers()
    main()
  except rospy.ROSInterruptException:
    pass