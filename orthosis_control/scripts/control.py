#!/usr/bin/python
""" This program subscribes and publishes to ros topics and has the controller.
    # Add more documentation here.
"""

__author__ = "Dhruv Kool Rajamani"
__email__ = "dkoolrajamani@wpi.edu"
__copyright__ = "Copyright 2019"
__date__ = "25 October 2019"

import sys

import rospy
from orthosis import Orthosis
from std_msgs.msg import Float64  # , String
from sensor_msgs.msg import JointState

import rbdl

import time

import numpy as np


class TorqueControl(Orthosis):
  """ Torque controller using M, C, G matrices """

  def __init__(self, namespace='/orthosis/', timestep=0.01, teleop=False):
    """ Initialize default timestep size """

    super(TorqueControl,
          self).__init__(
            namespace=namespace,
            timestep=timestep,
            teleop=teleop
          )

    self._gzserver_namespace = "/orthosis" + "/gzserver/"

    # self._gzserver_URI = int(
    #     rospy.get_param(self._gzserver_namespace + "URI"))

    rospy.Timer(rospy.Duration(self._timestep), self.control)
    return

  def control(self, event=None, verbose=False):
    if verbose:
      print(event)

    if not self.teleop:
      if not self.is_adaptive_control:
        self.inverseDynamicsController()
      else:
        self.adaptiveController()
    else:
      self.hwController()

    return

  def hwController(self):

    # CONVERT ALL STIFFNESS TO COMMON BETWEEN SIMULATION AND HARDWARE. ADD STIFFNESS
    # TO SIMULATION FOR INDEX FINGER AT LEAST. GOOD WAY TO TEST MATLAB MODEL SINCE
    # VELOCITIES AND ACCELERATIONS ARE MEASURABLE

    if self.teleop:
      # if not self.hw_enabled:
      #   return
      tau_comp = np.zeros((len(self._joints),))

      # Calculate error term and add to torque
      # for i, (k, v) in enumerate(self._q_des.items()):
      xMCP = self._joint_states[self._joints[0]][0]
      tauMCP = (0.006164 * ((xMCP * (180 / np.pi))**1)) - (
        0.000518 * ((xMCP * (180 / np.pi))**2)
      ) + (1.35472e-05 * ((xMCP * (180 / np.pi))**3)
           ) - (1.5868e-07 * ((xMCP * (180 / np.pi))**4)) + 0.145484517

      self._joint_states[self._joints[0]][2] = tauMCP
      kMCP = 1
      tau_comp[0] = kMCP * self._q_des[self._joints[0]] - tauMCP
      rospy.loginfo(tau_comp[0])

      xPIP = self._joint_states[self._joints[1]][0]
      tauPIP = (0.0134247 * ((xPIP * (180 / np.pi))**1)) - (
        0.001267 * (xPIP * (180 / np.pi))**2
      ) + (4.3943e-05 * ((xPIP * (180 / np.pi))**3)
           ) - (6.64656e-07 * ((xPIP * (180 / np.pi))**4)) + 0.03056009

      self._joint_states[self._joints[1]][2] = tauPIP
      kPIP = 1
      tau_comp[1] = (kPIP * self._q_des[self._joints[1]]) - tauPIP

      tau_comp_final = tau_comp[0] / 0.01326 if tau_comp[0] > tau_comp[
        1] else tau_comp[1] / 0.01457

      self.publishMotorEfforts(tau_comp_final / 0.65)  # 65% efficiency

    return

  def inverseDynamicsController(self):
    """ Body control """

    # self._is_init = False
    if self.sim_enabled:
      Kp = 0.01 * self._scale
      Kv = 0.0001 * self._scale

      is_set_point_ctrl = True

      self._finger_joints = [
        joint for joint in self._m_joints if joint.partition('_')[0] != 'arm'
      ]

      # Seperate Arm and Fingers
      for k, v in self._q_des.items():
        if "arm" in k:
          # extend the arm always?
          self._q_des[k] = self._joint_limits[k][1]

      # Calculate Gravity
      for k, v in self._states_map.items():
        self._q[v] = self._joint_states[k][0]
        self._qdot[v] = self._joint_states[k][1]
        # self._qdot[v] = 0.0
        self._qddot[v] = 0.0
      rbdl.InverseDynamics(
        self._model,
        self._q,
        self._qdot,
        self._qddot,
        self._tau
      )
      self._G = self._tau

      tau_compensated = np.zeros(self._model.qdot_size)
      q_comp = np.zeros(self._model.q_size)
      qdot_comp = np.zeros(self._model.q_size)

      # Calculate error term and add to torque
      for k, v in self._states_map.items():
        q_comp[v] = Kp * (self._q_des[k] - self._joint_states[k][0])
        qdot_comp[v] = -Kv * self._joint_states[k][1]
        tau_compensated[v] = self._G[v]
        if is_set_point_ctrl:
          tau_compensated[v] += q_comp[v] + qdot_comp[v]

      # print("Tau = " + str(tau_compensated.transpose()))

      self.publishJointEfforts(effort=True, cmd=tau_compensated)

    return

  def adaptiveController(self):

    Kp = 0.01 * self._scale
    Kv = 0.0001 * self._scale

    q_comp = [0., 0.]
    qdot_comp = [0., 0.]
    tau_arm = [0., 0.]

    # Seperate Arm and Fingers
    for i, k in enumerate(["arm_joint1", "arm_joint2"]):
      # extend the arm always?
      self._q_des[k] = self._joint_limits[k][1]
      q_comp[i] = Kp * (self._q_des[k] - self._joint_states[k][0])
      qdot_comp[i] = -Kv * self._joint_states[k][1]
      tau_arm[i] = q_comp[i] + qdot_comp[i]

    tau_compensated = self.tuneAdaptiveParams()

    tau = tau_arm
    tau.extend([tau_compensated[0][0], tau_compensated[0][1]])

    self.publishJointEfforts(effort=True, cmd=tau)

    return

  def toString(self):
    """ Serializes attributes of this class into a string """

    this_string = """\nNamespace = {}\nStep size = {}\n""".format(
      self._namespace,
      self._timestep
    )

    return this_string

  def runParameterIdentification(self):

    return


def generateControllerObjects(teleop):
  """ Generate objects of the controller specified """

  namespaces = []
  namespace = "/orthosis/"
  # islands = rospy.get_param("islands")
  # for island in range(1, 1 + islands):
  #   robots = rospy.get_param("/island_{}/robots".format(island))
  #   for robot_id in range(1, 1 + robots):
  #     namespaces.append(
  #       rospy.get_param("namespace_{}_{}".format(island,
  #                                                robot_id))
  #     )

  # robot_torque_controllers = {
  #   namespace: TorqueControl(namespace) for namespace in namespaces
  # }
  print "Generating TELEOP STATE: {}".format(teleop)
  robot_torque_controllers = {
    namespace: TorqueControl(namespace=namespace,
                             teleop=teleop)
  }

  return robot_torque_controllers


def main(teleop_state):
  """ main """

  teleop = teleop_state

  robot_torque_controllers = generateControllerObjects(teleop_state)

  rospy.spin()
  del robot_torque_controllers

  return


if __name__ == "__main__":
  try:
    if len(sys.argv) > 1:
      if sys.argv[1] is not None:
        teleop_state = int(sys.argv[1])

    if rospy.has_param("/orthosis/is_teleop"):
      teleop_state = rospy.get_param("/orthosis/is_teleop", False)

    rospy.loginfo("Generating TELEOP STATE: {}".format(bool(teleop_state)))
    rospy.loginfo(teleop_state)

    rospy.init_node("torque_control_py", anonymous=False)
    rospy.loginfo("ROS Serial Python Node")
    main(teleop_state)
  except rospy.ROSInterruptException:
    pass
