#!/usr/bin/python
""" This program subscribes to bend sensors and publishes to joint states
    # Add more documentation here.
"""

__author__ = "Dhruv Kool Rajamani"
__email__ = "dkoolrajamani@wpi.edu"
__copyright__ = "Copyright 2020"
__date__ = "05 February 2020"

import sys
import rospy
import rospkg
rospack = rospkg.RosPack()
rospack.list()
from std_msgs.msg import Float64  # , String
from sensor_msgs.msg import JointState
from bend_sensor_msg.msg import bend_sensor
import time
import numpy as np
import rospkg
import roslaunch
import subprocess
import os

root_path = rospack.get_path('orthosis_analysis')
fitting_path = root_path + '/scripts/'
sys.path.append(fitting_path)

from fitting import predict_values, model_load
from fitting import scale_sample, load_dataset
import tensorflow as tf
import keras

global X
global y
global ann_model
global session
global joints
global fingers
global joint_states
global positions
global velocities
global efforts
global bend_sensor_sub
global js_pub

X = None
y = None
ann_model = None
session = None
joints = None
fingers = None
joint_states = None
positions = None
velocities = None
efforts = None
bend_sensor_sub = None
js_pub = None


def returnFingerName(joint):

  finger = joint[:joint.find("_joint")]

  return finger


def bendSensorCb(msg, finger):
  """ Bend Sensor Callback """

  global X
  global y
  global ann_model
  global session
  global positions
  global velocities
  global efforts
  global joint_states
  global js_pub

  bend = msg.bend.data
  stretch = msg.stretch.data

  sample = np.ndarray(shape=(1, 2), dtype=float)
  sample = np.array([bend, stretch]).reshape((1, 2))

  # rospy.loginfo("Sample: {}".format(sample))

  with session.graph.as_default():
    keras.backend.set_session(session)

    if ann_model is None:
      rospy.logerr("ANN Model not initialized")
      ann_model = model_load()

    scaler = scale_sample()
    Xs = scaler.transform(sample)
    Y_predict = predict_values(Xs, ann_model)

    # assert Y_predict.graph is self._graph

    joint = ["{}_joint1".format(finger), "{}_joint2".format(finger)]

    positions[joint[0]] = Y_predict[0][0] * -np.pi / 180
    rospy.loginfo("Y_predict: {}".format(Y_predict * -np.pi / 180))
    positions[joint[1]] = Y_predict[0][1] * -np.pi / 180
    velocities[joint[0]] = 0.
    velocities[joint[1]] = 0.
    efforts[joint[0]] = 0.
    efforts[joint[1]] = 0.

    joint_states = {
      j: [positions[j],
          velocities[j],
          efforts[j]] for j in positions.keys()
    }

    msgMain = JointState()
    msgMain.header.stamp = rospy.Time.now()
    msgMain.name = joint
    msgMain.position = positions.values()
    msgMain.velocity = velocities.values()
    msgMain.effort = efforts.values()
    js_pub.publish(msgMain)

  return


def initialize_model():
  global X
  global y
  global ann_model
  global session

  X, y = load_dataset([9])
  graph = tf.Graph()
  session = tf.compat.v1.Session(graph=graph)
  with session.graph.as_default():
    keras.backend.set_session(session)
    ann_model = model_load()

  return


def initialize_publishers_and_subscribers():
  """ Subscribe to joints coming from bend sensors """
  global positions
  global velocities
  global efforts
  global joint_states
  global fingers
  global joints
  global js_pub

  fingers_set = set([returnFingerName(joint) for joint in joints])
  fingers = list(fingers_set)
  bend_sensor_sub = [
    rospy.Subscriber(
      "/devices/{}/bend_sensor".format(finger),
      bend_sensor,
      bendSensorCb,
      finger,
      10
    ) for finger in fingers
  ]

  # Creating a subscriber
  js_pub = rospy.Publisher("/orthosis/joint_states", JointState, queue_size=10)

  positions = {}
  velocities = {}
  efforts = {}
  for joint in joints:
    positions.update({joint: 0.})
    velocities.update({joint: 0.})
    efforts.update({joint: 0.})

  joint_states = {
    joint: [positions[joint],
            velocities[joint],
            efforts[joint]] for joint in positions.keys()
  }

  return


if __name__ == "__main__":
  try:
    global joints

    rospy.init_node("flex_to_joint_adapter_py", anonymous=False)
    rospy.loginfo("flex_to_joint_adapter_py has been initialized")
    joints = rospy.get_param(
      "/orthosis/hw_config_joints",
      ["index_joint1",
       "index_joint2"]
    )
    initialize_model()
    initialize_publishers_and_subscribers()

    while not rospy.is_shutdown():
      rospy.spin()

  except rospy.ROSInterruptException:
    pass