#!/usr/bin/python
""" This program subscribes and publishes to ros topics and creates a base robot
class that can be later created for other controllers
    # Add more documentation here.
"""

__author__ = "Dhruv Kool Rajamani"
__email__ = "dkoolrajamani@wpi.edu"
__copyright__ = "Copyright 2019"
__date__ = "25 October 2019"

import sys, os

import rospy
from std_msgs.msg import Float64, Float32, Bool, Float32MultiArray  # , String
from sensor_msgs.msg import JointState
from urdf_parser_py import urdf
from motor_msg.msg import motor_desired, motor_measured
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

import rbdl
import rospkg

import numpy as np


class Orthosis(object):
  """ Default class to subscribe and publish to orthosis robots """

  def __init__(self, namespace="/", timestep=0.01, teleop=None):
    """ Initialize the class with the namespace and timestep as params """

    super(Orthosis, self).__init__()
    self.teleop = None
    if teleop is not None:
      self.teleop = teleop
    else:
      self.teleop = rospy.get_param("/orthosis/is_teleop")
    self._namespace = namespace
    self._timestep = timestep

    self._teleop_sub = rospy.Subscriber(
      "/teleop_state",
      Bool,
      self.teleopStateCb
    )

    self.hw_enabled = False
    self.sim_enabled = False
    self.is_adaptive_control = False
    self.activate_adaptive_controller = False
    self.adaptive_controller_state_sub = rospy.Subscriber(
      self._namespace + "activate_adaptive_controller",
      Bool,
      self.adaptiveControllerStateCb
    )

    self._sim_sub = rospy.Subscriber("/sim_state", Bool, self.simStateCb)

    self._fingers = ["index", "middle", "ring", "pinky"]  #, "thumb"]
    self._active_fingers = ["index"]
    self._n_joints = int(rospy.get_param(self._namespace + "n_joints"))

    # Movable joints
    self._m_joints = rospy.get_param(
      self._namespace + "parameters/control/joints"
    )
    # self._n_joints = len(self._m_joints)

    self._m_joints_dict = {
      typ: rospy.get_param(self._namespace + "all_joints/{}".format(typ))
      for typ in urdf.Joint.TYPES
    }

    for k, v in self._m_joints_dict.items():
      if len(v) < 1 or k == 'fixed' or k == 'floating' or k == 'unknown':
        del self._m_joints_dict[k]

    self._links = {}
    for typ, joints in self._m_joints_dict.items():
      for joint_i, joint in enumerate(joints):
        if joint not in self._links.keys():
          self._links[joint] = rospy.get_param(
            self._namespace + "joints/{}/{}".format(typ,
                                                    joint_i)
          )

    self._joint_states = {
      joint: [1.0,
              2.0,
              3.0,
              4.0] for joint_i,
      joint in enumerate(self._m_joints)
    }

    self._past_joint_states = {
      joint: [[1.0,
               2.0,
               3.0,
               4.0] for i in range(5)] for joint_i,
      joint in enumerate(self._m_joints)
    }

    self._joint_states_count = [0 for joint in self._m_joints]

    self._joint_limits = {
      joint:
      rospy.get_param(self._namespace + "joint/limits/{}".format(joint))
      for joint in self._m_joints
    }

    self._q_des = {
      joint: self._joint_limits[joint][0] for joint in self._m_joints
    }

    self._dq_des = {joint: 0. for joint in self._m_joints}

    self._ddq_des = {joint: 0. for joint in self._m_joints}

    self.q = {joint: self._joint_limits[joint][0] for joint in self._m_joints}

    self.dq = {joint: 0. for joint in self._m_joints}

    self.ddq = {joint: 0. for joint in self._m_joints}

    self.sliding_surface = []

    self._q_des_hw = {
      joint: self._joint_limits[joint][0] for joint in self._m_joints
    }

    self._joints = rospy.get_param(
      self._namespace + "hw_config_joints",
      ["index_joint1",
       "index_joint2"]
    )

    self._joint_states_hw = {
      joint: [0.0,
              0.0,
              0.0] for joint_i,
      joint in enumerate(self._joints)
    }

    if self.teleop:
      rospy.loginfo("INITIALIZING TELEOP MODE")
      self.initialize_hw_control()
      self.hw_enabled = True
    elif not self.teleop:
      print "INITIALIZING SIMULATION MODE"
      self.initialize_sim_control()
      self.initAdaptiveControlScheme(rospy.Time.now().to_sec())
      self.sim_enabled = True

    return

  def adaptiveControllerStateCb(self, msg):

    self.activate_adaptive_controller = msg.data
    self.is_adaptive_control = msg.data

    return

  def initAdaptiveControlScheme(self, _t):

    self.joint_trajectory_pub = rospy.Publisher(
      self._namespace + "joint_trajectories",
      JointTrajectory,
      queue_size=10
    )

    self.Kp = {
      finger: 0.01 * self._scale * np.eye(2) for finger_i,
      finger in enumerate(self._active_fingers)
    }

    self.Kv = {
      finger: 0.0001 * self._scale * np.eye(2) for finger_i,
      finger in enumerate(self._active_fingers)
    }

    self.excitation_frequency = 5

    self.trajectory_coeffs = {
      joint:
      ((self._joint_limits[joint][1] - self._joint_limits[joint][0]) / 2)
      for joint in self._m_joints
    }

    # Initial Estimates:
    self.gamma = 50 * np.eye(5)
    self.alpha = [
      np.array([0.1,
                0.01,
                0.001,
                0.0001,
                1]).reshape(5,
                            1) for i in self._active_fingers
    ]

    self._M = [0.0000015 * np.ones((2, 2)) for i in self._active_fingers]
    self._V = [0.000015 * np.ones((2, 2)) for i in self._active_fingers]
    self._s = [0.1 for i in self._active_fingers]

    self.Y = [np.zeros((2, 5)) for i in self._active_fingers]

    self.start_time = _t
    self.step_time = 0.01
    self.previous_time = _t

    self.sliding_surface = [
      np.array([0.,
                0.]).reshape(2,
                             1) for i in self._active_fingers
    ]

    self._x_init = self.getStates(_t)

    return

  def getStates(self, _t):

    for joint_i, joint in enumerate(self._m_joints):
      self._q_des[joint] = self.trajectory_coeffs[joint] * np.sin(
        self.excitation_frequency * _t
      ) - ((self._joint_limits[joint][1] - self._joint_limits[joint][0]) / 2)
      self._dq_des[joint] = self.excitation_frequency * self.trajectory_coeffs[
        joint] * np.cos(self.excitation_frequency * _t)
      self._ddq_des[joint] = -(self.excitation_frequency**
                               2) * self.trajectory_coeffs[joint] * np.sin(
                                 self.excitation_frequency * _t
                               )

      self.q[joint] = self._joint_states[joint][0]
      self.dq[joint] = self._joint_states[joint][1]
      self.ddq[joint] = self._joint_states[joint][3]

    x_des = []
    for finger_i, finger in enumerate(self._active_fingers):
      joint = ["{}_joint1".format(finger), "{}_joint2".format(finger)]
      d = np.array(
        [
          self._q_des[joint[0]],
          self._q_des[joint[1]],
          self._dq_des[joint[0]],
          self._dq_des[joint[1]]
        ]
      ).reshape(4,
                1)
      x_des.append(d)

      e1 = self.q[joint[0]] - self._q_des[joint[0]]
      e2 = self.q[joint[1]] - self._q_des[joint[1]]
      de1 = self.dq[joint[0]] - self._dq_des[joint[0]]
      de2 = self.dq[joint[1]] - self._dq_des[joint[1]]
      dde1 = self.ddq[joint[0]] - self._ddq_des[joint[0]]
      dde2 = self.ddq[joint[1]] - self._ddq_des[joint[1]]

      self.sliding_surface[finger_i] = np.array([e1, e2]).reshape(2, 1)

      self.Y[finger_i][0] = np.array(
        [1.,
         self.q[joint[0]],
         self.q[joint[0]]**2,
         self.q[joint[0]]**3,
         dde1]
      )

      self.Y[finger_i][1] = np.array(
        [1.,
         self.q[joint[1]],
         self.q[joint[1]]**2,
         self.q[joint[1]]**3,
         dde2]
      )

    return x_des

  def tuneAdaptiveParams(self):
    u = []

    _t = rospy.Time.now().to_sec()
    if (
      self.previous_time > 0.1 and self.previous_time < 0.001
    ) or self.activate_adaptive_controller:
      self.step_time = 0.01
      self.start_time = _t
      self.activate_adaptive_controller = False

    else:
      self.step_time = _t - self.previous_time
    self.getStates(_t)

    for finger_i, finger in enumerate(self._active_fingers):
      y_a = -(np.linalg.inv(self.gamma).dot(self.Y[finger_i].T))
      alpha_gradient = y_a.dot(self.sliding_surface[finger_i])
      self.alpha[finger_i] += alpha_gradient * self.step_time

      u.append(self.Y[finger_i].dot(self.alpha[finger_i]))

    self.previous_time = _t
    return u

  def teleopStateCb(self, data):
    self.teleop = data.data
    if self.teleop == True:
      rospy.loginfo("INITIALIZING TELEOP MODE")
      self.initialize_hw_control()
      self.hw_enabled = True
    else:
      self.hw_enabled = False

  def simStateCb(self, data):
    if data.data:
      rospy.loginfo("INITIALIZING SIM MODE")
      self.initialize_sim_control()

    return

  def initialize_hw_control(self):

    self._motor_pub = rospy.Publisher(
      "/devices/index/motor_desired",
      motor_desired,
      queue_size=10
    )

    self.subToJointStates()
    self.subToCommandGUI()

    self.hw_enabled = True

    return

  def initialize_sim_control(self):
    self._default_pub_rate = 100
    self._model_loaded = False

    self.initializeRBDLModel()

    self._M = np.zeros([self._n_joints, self._n_joints])
    self._C = np.zeros([self._n_joints, self._n_joints])

    self._scale = rospy.get_param(self._namespace + "scale")

    # Init joint states to zero

    self._finger_states = {
      "index":
        {
          "index_joint1": [11.0,
                           self._joint_limits["index_joint1"][0]],
          "index_joint2": [9.0,
                           self._joint_limits["index_joint2"][0]]
        },
      "middle":
        {
          "middle_joint1": [11.0,
                            self._joint_limits["middle_joint1"][0]],
          "middle_joint2": [9.0,
                            self._joint_limits["middle_joint2"][0]]
        },
      "ring":
        {
          "ring_joint1": [11.0,
                          self._joint_limits["ring_joint1"][0]],
          "ring_joint2": [9.0,
                          self._joint_limits["ring_joint2"][0]]
        },
      "pinky":
        {
          "pinky_joint1": [11.0,
                           self._joint_limits["pinky_joint1"][0]],
          "pinky_joint2": [9.0,
                           self._joint_limits["pinky_joint2"][0]]
        }
      #   ,
      # "thumb":
      #   {
      #     "mcp_joint": [11.0,
      #                   self._joint_limits["index_joint1"][0]],
      #     "pip_joint": [9.0,
      #                   self._joint_limits["index_joint1"][0]]
      #   }
    }

    # print self._joint_limits

    # Setup effort publishers
    self._effort_flag = bool(
      rospy.get_param(self._namespace + "is_effort_enabled")
    )
    if self._effort_flag:
      self._joint_effort_pubs = {
        joint: rospy.Publisher(
          (
            self._namespace +
            "control/config/joint_effort_controller_joint_{}/command"
            .format(joint_i)
          ),
          Float64,
          queue_size=10
        ) for joint_i,
        joint in enumerate(self._m_joints)
      }
    else:
      self._joint_effort_pubs = {
        joint: rospy.Publisher(
          (
            self._namespace +
            "control/config/joint_position_controller_joint{}/command"
            .format(joint_i)
          ),
          Float64,
          queue_size=10
        ) for joint_i,
        joint in enumerate(self._m_joints)
      }

    # Publish initial efforts to reach home position?
    for k, v in self._joint_effort_pubs.items():
      cmd = 0
      self._joint_effort_pubs[k].publish(cmd)

    self.subToJointStates()
    self.subToCommandGUI()

    self.sim_enabled = True

    return

  def guiCommandCb(self, data, finger):
    """ Desired Joint Position Callback """

    # Should convert to trajectory and IK instead.

    joint = ["{}_joint1".format(finger), "{}_joint2".format(finger)]

    self._q_des[joint[0]] = (data.data / 1000.0) * (
      self._joint_limits[joint[0]][0]
    )
    self._q_des[joint[1]] = (data.data / 1000.0) * (
      self._joint_limits[joint[1]][0]
    )

    return

  def jointStatesCb(self, data):
    """ Joint States Callback """
    # joints = self._joints if self.teleop and not self.sim_enabled else self._m_joints
    joints = self._m_joints
    for joint_i, joint in enumerate(joints):
      if not self.teleop:
        index = data.name.index(joint)
        msg = [data.position[index], data.velocity[index], data.effort[index]]

        if self._joint_states_count[joint_i] < 5:
          self._past_joint_states[joint][0] = msg[0]
          self._past_joint_states[joint][1] = msg[1]
          self._past_joint_states[joint][2] = msg[2]
          self._joint_states_count[joint_i] += 1
        else:
          self._past_joint_states[joint][0] = self._past_joint_states[joint][1]
          self._past_joint_states[joint][1] = self._past_joint_states[joint][2]
          self._past_joint_states[joint][2] = self._past_joint_states[joint][3]
          self._past_joint_states[joint][3] = self._past_joint_states[joint][4]
          self._past_joint_states[joint][4][0] = msg[0]
          self._past_joint_states[joint][4][1] = msg[1]
          self._past_joint_states[joint][4][2] = msg[2]
          self._past_joint_states[joint][4][
            3] = msg[1] - self._past_joint_states[joint][3][1]

        self._joint_states[joint] = self._past_joint_states[joint][-1]

      else:
        if joint in ["arm_joint1", "arm_joint2"]:
          index = data.name.index(joint)
          msg = [
            data.position[index],
            data.velocity[index],
            data.effort[index]
          ]
          self._joint_states[joint] = msg

        if joint in self._joints:
          index = data.name.index(joint)
          msg = [
            data.position[index],
            data.velocity[index],
            data.effort[index]
          ]
          self._joint_states[joint] = msg
        else:
          self._joint_states[joint][1] = 0.0
          self._joint_states[joint][2] = 0.0
          self._joint_states[joint][0] = self._joint_limits[joint][0]

    return

  def jointStatesHwCb(self, data, joint):
    """ Joint States Callback """

    self._joint_states_hw[joint] = [
      data.position[0],
      data.velocity[0],
      data.effort[0]
    ]
    self._joint_states[joint] = self._joint_states_hw[joint]

    if self.teleop:
      self._q_des[joint] = data.position[0]

    return

  def subToCommandGUI(self):
    """ Subscribe to desired percentage flexion coming from GUI """

    self._q_des_sub = [
      rospy.Subscriber(
        "{}{}".format(self._namespace,
                      finger),
        Float32,
        self.guiCommandCb,
        finger
      ) for finger in self._fingers
    ]
    return

  def subToHwJointStates(self):
    """ Subscribe to joint state messages if published """

    self._joint_states_hw_sub = [
      rospy.Subscriber(
        self._namespace + "joint_states_hw/{}".format(joint),
        JointState,
        self.jointStatesHwCb,
        joint
      ) for joint in self._joints
    ]

  def subToJointStates(self):
    """ Subscribe to joint state messages if published """

    self._joint_states_sub = rospy.Subscriber(
      self._namespace + "joint_states",
      JointState,
      self.jointStatesCb
    )

    return

  def publishJointEfforts(self, cmd=None, effort=True):
    """ Publish cmd[arr] values to joints """
    if not self.teleop:
      if cmd is None:
        cmd = [0 for i, (k, v) in enumerate(self._joint_effort_pubs)]

        for i, (k, v) in enumerate(self._joint_effort_pubs.items()):
          cmd[i] = 0.01 * np.sin(rospy.Time.now().to_sec() * np.pi / 2) * 1
          # print str(i) + " : " + str(cmd[i])
          self._joint_effort_pubs[k].publish(cmd[i])

      else:
        for i, c in enumerate(cmd):
          k = self._states_map.keys()[self._states_map.values().index(i)]
          self._joint_effort_pubs[k].publish(cmd[i])

    return

  def publishMotorEfforts(self, cmd=None):
    # REPLACE WITH REVERSE EQUATION FOR LEADSCREW TO MOTOR
    tau_max = 1
    msg = motor_desired()
    torque_constant = 10.9
    cmd_dir = 1 if cmd > 0 else -1
    # direction = 1 if (data.data > 50) else -1
    if tau_max - np.abs(cmd) < 0:
      tau_max = np.abs(cmd)
    # tau = (cmd/tau_max) * 6.4
    msg.desired_force.data = cmd
    msg.torque_constant.data = torque_constant
    msg.header.frame_id = "index"
    msg.header.stamp = rospy.Time.now()
    self._motor_pub.publish(msg)

    return

  def initializeRBDLModel(self):
    """ Load the URDF model using RBDL """

    rospack = rospkg.RosPack()
    rospack.list()

    root_path = rospack.get_path('orthosis_description')
    model_path = root_path + "/urdf/orthosis_description.urdf"
    # Create a new model
    self._model = rbdl.loadModel(model_path)

    self._q = np.zeros(self._model.q_size)
    self._qdot = np.zeros(self._model.qdot_size)
    self._qddot = np.zeros(self._model.qdot_size)
    self._tau = np.zeros(self._model.qdot_size)
    self._G = np.zeros(self._model.qdot_size)

    self._states_map = {
      joint: self._model.mJoints[self._model.GetBodyId(link[1])].q_index
      for joint,
      link in self._links.items()
    }

    return